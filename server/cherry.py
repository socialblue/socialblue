from pymongo import *
from datetime import *
from time import *
import cherrypy
from datetime import datetime
import json

from cherrypy.process import servers
def fake_wait_for_occupied_port(host, port): return
servers.wait_for_occupied_port = fake_wait_for_occupied_port

#Metodo Registro de usuario
@cherrypy.expose
def shutdown(self):
    cherrypy.engine.exit()



def registro_usuario(nombre,email,contrasena,estado,telefono,foto,mac):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    registro ={"_id": mac,"name": nombre,"visibilidad":0,"fec_ult_mod":datetime.utcnow(),"profile":{"email" :email,"contrasena":contrasena,"estado": estado,"mac":mac ,$
    a=user.insert(registro)
    if(a==None):
        return  False
    else :
        return True
#modificar politicas privacidad
def politica(mac,estado):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection()
    db = conexion['user']
    user = db['col_user'] #coleccion con registros de usuarios
    user.update({'_id':mac},{"$set":{'visibilidad':estado}})
#Metodo de autentificacion
def login (mac,email,contrasena):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    a=user.find_one({'_id':mac,'profile.email':email,'profile.contrasena':contrasena})
    print(a)
    if(a==None):
        return  False#false
    else :
        return True #true
#127.0.0.1:8080/api/metodos/1/?param_1=80:70:40:50
#Metodo de descargar perfil del usuario registrado
def buscar_amigo(mac):

    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    a=user.find_one({'_id':mac},{'visibilidad':1,'profile.foto':1,'name':1,'profile.estado':1,'profile.megusta':1,'profile.megustadiario':1,'Megusta':1})
    if (a!=None):
        if(a['visibilidad']==1):
            return None
    return a
buscar_amigo.exposed=True
# modifica coleccion visto
def anadir_lista_visto(mac_source,mac_target):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    #esta_megusta=user.find_one({'_id':mac_source,'Megusta':mac_target})
    #print(esta_megusta)
    #if(esta_megusta!=None):

    lista_visto = db['col_estuvimos_juntos']
    registro ={"_id": mac_source,"mac_vista":[{"mac":mac_target,"fechas":[]}]}
    lista_visto.insert(registro)

    lista_visto.update({'_id':mac_source},{"$addToSet":{'mac_vista.0.fechas':  datetime.utcnow()}})
'''    registro ={"_id": mac_source, "mac_target":mac_target,"listafechas":[]}
    lista_visto.insert(registro)
    #lista_visto.update({'_id':mac_source,"mac_target":mac_target},{"$addToSet":{'listafechas.0.fechas':datetime.utcnow()}})
    '''

    #return esta_megusta
#Metodo que devuelve las mac que ha visto
def visto(mac_source,mac_target):
    conexion = Connection() # Conexion local por defecto
    db = conexion['user'] #base de datos a usar
    tevisto = db['col_estuvimos_juntos']
    #respuesta=tevisto.find_one({'_id':mac_source,'mac_target':mac_target})
    #respuesta=tevisto.find_one({'_id':mac_source})
    respuesta=tevisto.find_one({'_id':mac_source,'mac_vista.mac':mac_target},{'mac_vista.fechas':1})

    if(respuesta==None):
        return False
    else :
        return respuesta


#funcion para que podamos devolver fechas en json
def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj



#ver perfil
def buscar_perfil(mac):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    #a=user.find_one({'_id':mac},{'visibilidad':1,'profile.foto':1,'name':1,'profile.estado':1,'profile.megusta':1,'profile.megustadiario':1,'fec_ult_mod':1})
    a=user.find_one({'_id':mac},{'visibilidad':1,'profile.foto':1,'name':1,'profile.estado':1,'profile.megusta':1,'profile.email':1,'profile.megustadiario':1})
    return a
# Metodo modificar visibilidad 0 se muestra para todo el mundo 1 no es visible para nadie
def mod_visibilidad(mac,visibilidad):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    user.update({'_id':mac},{"$set":{'visibilidad':visibilidad}})
#modificar foto
def mod_foto(mac,foto):
    print ("Conectando al Servidor de Base de Datos Local...")
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con registros de usuarios
    user.update({'_id':mac},{"$set":{'profile.foto':foto}})
#modificar estado
def mod_estado(mac,estado,nombre):
    conexion = Connection()
    db = conexion['user']
    user = db['col_user'] #coleccion con registros de usuarios
    user.update({'_id':mac},{"$set":{'profile.estado':estado,'name':nombre}})
    #user.update({'_id':mac},{"$set":{'name':nombre}})

def mod_contrasena(mac,contrasena):
    conexion = Connection()
    db = conexion['user']
    user = db['col_user'] #coleccion con registros de usuarios
    user.update({'_id':mac},{"$set":{'profile.contrasena':contrasena}})
    #user.update({'_id':mac},{"$set":{'name':nombre}})




# metodo que incrementa el contador megusta
def megusta(target,source):
    conexion = Connection() # Conexion local por defecto
    #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
    #creando/obteniendo un objeto que referencie a la base de datos.
    db = conexion['user'] #base de datos a usar
    #creando/obteniendo un objeto que referencie a la coleccion.
    user = db['col_user'] #coleccion con re
    insertar_megusta_diario(source,target)
    user.update({'_id':target},{"$inc":{'profile.megusta':1}})
    user.update({'_id':target},{"$inc":{'profile.megustadiario':1}})
    user.update({'_id':source},{"$addToSet":{'Megusta':target}})

    #metodo que guarda registro de megusta
def insertar_megusta_diario(mac_Source,mac_target):
    conexion = Connection() # Conexion local por defecto
    db = conexion['user'] #base de datos a usar
    col_megusta = db['col_megusta_diario']
    registro ={'mac_Source':mac_Source,'mac_target':mac_target,'fecha_ultimo_megusta':datetime.utcnow()}
    col_megusta.insert(registro)
#comprueba si ha pasado 24 horas desde el ultimo megusta
def Comprobar_megusta_diario(mac_Source,mac_target):
     conexion = Connection() # Conexion local por defecto
     #conexion = Connection("usuario:contrasena@servidor.com:27075/basededato") #Conexion a un servidor remoto
     #creando/obteniendo un objeto que referencie a la base de datos.
     db = conexion['user'] #base de datos a usar
     #creando/obteniendo un objeto que referencie a la coleccion.
     col_megusta = db['col_megusta_diario']
     fecha=col_megusta.find_one({'mac_target':mac_target,'mac_Source':mac_Source},{'fecha_ultimo_megusta':1,'_id':0})
     fecha_actual=datetime.utcnow()
     #print(fecha['fecha_ultimo_megusta'])
     resta_dias=fecha_actual-fecha['fecha_ultimo_megusta']
     if (resta_dias.days<=1):
        return True
     else:
	return False

#Bloquear source bloquea a target
def bloquear2(mac_source,mac_target):
    conexion = Connection() # Conexion local por defecto
    db = conexion['user'] #base de datos a usar
    bloquear = db['col_bloquear']
    a= bloquear.insert({'mac_source':mac_source, 'mac_target':mac_target})


def bloquear(mac_source,mac_target):
    conexion = Connection() # Conexion local por defecto
    db = conexion['user'] #base de datos a usar
    bloquear = db['col_bloquear']
    insertar ={'_id':mac_source},{'mac_target':[]}
    bloquear.insert(insertar)
    bloquear.update({'_id':mac_source},{"$addToSet":{'mac_target':mac_target}})
#Desbloquear source bloquea a target
def desbloquear(mac_source,mac_target):
    conexion = Connection() # Conexion local por defecto
    db = conexion['user'] #base de datos a usar
    bloquear = db['col_bloquear']
    registro= bloquear.update({'_id':mac_source},{'$pull':{'mac_target':mac_target}})

#esta bloqueado?
def esta_bloqueado(mac_source,mac_target):
    conexion=Connection()
    db = conexion['user'] #base de datos a usar
    bloquear = db['col_bloquear']
    respuesta=bloquear.find_one({'_id':mac_source,'mac_target':mac_target})
    if(respuesta==None):
        return False
    else :
        return True
def listadobloqueados(mac_source):
    conexion = Connection() # Conexion local por defecto
    db = conexion['user'] #base de datos a usar
    bloquear = db['col_bloquear']
    a=bloquear.find_one({'_id':mac_source})
    if (a==None):
        return False
    else:
        return a
class Metodos :
    exposed = True
    login.exposed=True
    megusta.exposed=True
    buscar_amigo.exposed=True
    registro_usuario.exposed=True
    buscar_perfil.exposed=True
    Comprobar_megusta_diario.exposed=True
    bloquear.exposed=True
    desbloquear.exposed=True
    listadobloqueados.exposed=True
 def GET(self, id=None,param_1=None,nombre=None,email=None,contrasena=None,estado=None,telefono=None,foto=None,mac=None,mac_source=None,mac_target=None):
            if id == None:
               return('No song with the IDnone' )
            elif id =='buscaramigo':
               return json.dumps(buscar_amigo(cherrypy.request.params.get("mac")))
            elif id =='login':
               e=cherrypy.request.params.get("email")
               c=cherrypy.request.params.get("contrasena")
               m=cherrypy.request.params.get("mac")
               return json.dumps(login(m,e,c))
            elif id =='registro':
               n=cherrypy.request.params.get("nombre")
               e=cherrypy.request.params.get("email")
               c=cherrypy.request.params.get("contrasena")
               es=cherrypy.request.params.get("estado")
               t=cherrypy.request.params.get("telefono")
               f=cherrypy.request.params.get("foto")
               m=cherrypy.request.params.get("mac")
               return json.dumps(registro_usuario(n,e,c,es,t,f,m))
            elif id=='perfil':
               return json.dumps(buscar_perfil(cherrypy.request.params.get("mac")))
            elif id=='megusta':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               json.dumps(megusta(target,source))
            elif id =='comprobarDiario':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               return json.dumps(Comprobar_megusta_diario(source,target))
            elif id=='bloquear':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               json.dumps(bloquear(source,target))
            elif id=='desbloquear':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               json.dumps(desbloquear(source,target))
            elif id=='bloqueado':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               return json.dumps(esta_bloqueado(source,target))
            elif id=='visto':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               json.dumps(anadir_lista_visto(source,target))
            elif id=='devuelvevisto':
               target=cherrypy.request.params.get("mac_target")
               source=cherrypy.request.params.get("mac_source")
               return json.dumps(visto(source,target),default=date_handler)
            elif id=='modfoto':
               source=cherrypy.request.params.get("mac")
               estado=cherrypy.request.params.get("estado")
               json.dumps(mod_foto(source,estado))
            elif id=='modestado':
               source=cherrypy.request.params.get("mac")
               estado=cherrypy.request.params.get("estado")
               nombre=cherrypy.request.params.get("nombre")
               json.dumps(mod_estado(source,estado,nombre))
            elif id=='mod_contrasena':
               source=cherrypy.request.params.get("mac")
               contrasena=cherrypy.request.params.get("contrasena")
               json.dumps(mod_contrasena(source,contrasena))
            elif id=='listadobloqueados':
               source=cherrypy.request.params.get("mac_source")
               return json.dumps(listadobloqueados(source))
            elif id== 'politica':
               source=cherrypy.request.params.get("mac")
               estado=cherrypy.request.params.get("estado")
               json.dumps(politica(source,estado))
            elif id== 'x':
               shutdown(self)
            else:
               return('Vacio' )

if __name__ == '__main__':

        cherrypy.tree.mount(
            Metodos(), '/api/metodos',
            {'/':
                {'request.dispatch': cherrypy.dispatch.MethodDispatcher()}
            }
	}
        cherrypy.config.update({'server.socket_host': '172.31.33.19',
                        'server.socket_port': 80,
                        'tools.encode.on': True,
                        'toots.encode.encoding': 'utf-8',
                        'tools.decode.on': True,

                       })



        cherrypy.engine.start()
        cherrypy.engine.block()



