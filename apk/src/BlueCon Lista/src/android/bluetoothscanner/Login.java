package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import android.net.nsd.NsdManager.RegistrationListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {

	
	private EditText email,contrasena;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	
		final Button aceptar = (Button)findViewById(R.id.enviar);
		final Button registrate = (Button)findViewById(R.id.registrate);
		final CheckBox recordar=(CheckBox )findViewById(R.id.recordar);
		
		
		 email=(EditText)findViewById(R.id.email);
		 contrasena=(EditText)findViewById(R.id.contrasena);
		
		aceptar.setOnClickListener(new OnClickListener()
		{
	        public void onClick(View v)
	        {
	         	String ema=email.getText().toString();
	         	String con=contrasena.getText().toString();
	         	String pepe;
	         
	         	pepe=GET("http://192.168.1.102:64444/api/metodos/login?email="+ema+"&contrasena="+con);
	    		JSONObject json = null;
				try {
					
					json = new JSONObject(pepe);
					
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    		//reg.setText(json.toString(1));
				aceptar.setText(pepe);
     		
				//Si esta seleccionado lo guardo en sqlite el email y contrase�a
				


			if(pepe.startsWith("t"))
			{
				Toast.makeText(v.getContext(), "Server OK", Toast.LENGTH_SHORT).show();
	        	
	        	if(recordar.isChecked()) //Si esta seleccionado el tic, se almacena en sqlite su email y pass
				{
					sqlite admin=new sqlite(v.getContext(), "recordar", null, 1);
				       
				       SQLiteDatabase db=admin.getWritableDatabase();
				       ContentValues registro=new ContentValues();
				       
				       registro.put("email",ema);
				       registro.put("contrasena",con);
				       db.insert("recordatorio", null, registro);

				       db.close();
				       
				       Toast.makeText(v.getContext(), "Recordado con exito", Toast.LENGTH_SHORT).show();
				}
	        	
	        	Intent inten = new Intent(Login.this,BluetoothScannerActivity.class);
	        	startActivity(inten);
				
				
			}else
			{
				Toast.makeText(Login.this, "Usuario o contrase�a incorrectas", Toast.LENGTH_LONG).show();
				email.setText("");
	         	contrasena.setText("");
				
			}
     		
     		
        }
       
    });
		
		
		//Boton de registrate, este boton pasa a la otra pantalla del registro
		registrate.setOnClickListener(new OnClickListener()
		{
	        public void onClick(View v)
	           {
	        		// Pantalla registro
	            	Intent inten2 = new Intent(Login.this,Registro.class);
	           	 	startActivity(inten2);

	           }
		
		 });
		
		
		
		
		
}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}

	
	
	
	
	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

	          // receive response as inputStream
	          inputStream = httpResponse.getEntity().getContent();

	          // convert inputstream to string
	          if(inputStream != null)
	              result = convertInputStreamToString(inputStream);
	          else
	              result = "Did not work!";

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }

	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;

	  }
	  
	  
	
	
	
	
}
