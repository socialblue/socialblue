package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Perfil extends Activity {
       String macSource="";
       String macDestino="";
       TextView contador;
       
       Object a = null;
       
       
        @Override
        public void onCreate(Bundle savedInstanceState) {
        	super.onCreate(savedInstanceState);
        	requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.activity_perfil);
	       
	        
	        
	        

   		 if (!verificaConexion(this)) {
   			    Toast.makeText(getBaseContext(),
   			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();        			    
   			}
   		 else
   		 {
	        
	        Intent in = getIntent();
	        final String TAG_MAC = "mac";
	        JSONObject json = null;
	        JSONObject json2 = null;
	        String nom = null,est=null,cont=null, imagen=null;
	        JSONArray getJSONArray = null;
	        JSONArray array = null;
	        
	      
	        final TextView nombre = (TextView) findViewById(R.id.nombre);
	        TextView estado = (TextView) findViewById(R.id.estado);
	        contador = (TextView) findViewById(R.id.contador);
	        TextView juntos= (TextView) findViewById(R.id.juntos);
	        ImageView im= (ImageView) findViewById(R.id.foto);
	        
	        TextView etinombre=(TextView) findViewById(R.id.etinombre);
	        TextView etiestado=(TextView) findViewById(R.id.etiestado);
	      //  TextView etimasdias=(TextView) findViewById(R.id.etidias);
	        TextView etiultimo=(TextView) findViewById(R.id.etiultimodia);
	       // TextView etimegustas=(TextView) findViewById(R.id.etimegustas);
	       // TextView etiblo=(TextView) findViewById(R.id.etibloquear);
	        
	        
	      
	        
	        Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
			nombre.setTypeface(font);
			estado.setTypeface(font);
			contador.setTypeface(font);
			juntos.setTypeface(font);
			//etiblo.setTypeface(font);
			//etimegustas.setTypeface(font);
			etiultimo.setTypeface(font);
			//etimasdias.setTypeface(font);
			etiestado.setTypeface(font);
			etinombre.setTypeface(font);
	        
	        
	        
	        final ImageView mg= (ImageView) findViewById(R.id.gus);
	        ImageView blo= (ImageView) findViewById(R.id.ImageView1);
	      //  Button des= (Button) findViewById(R.id.des);
	       ImageView masdias= (ImageView) findViewById(R.id.mas);
	        
	       String pepe,visto;
	        // Displaying all values on the screen
	        
	        
	    	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    	macSource= mBluetoothAdapter.getAddress();
	        macDestino=in.getStringExtra(TAG_MAC);
	        
	        
	        
	      //Consulto la tabla de te he visto y muestro los resultados en un campo de texto
	      visto=GET("http://54.72.215.193/api/metodos/devuelvevisto/?mac_source="+macSource+"mac_target="+macDestino);
	     
	      
	      try 
	      {
	         json = new JSONObject(visto);
	      } 
	      catch (JSONException e) 
	      {
	         e.printStackTrace();
	      }


	         try 
	         {
	        	 array = json.getJSONArray(("mac_vista"));
			} 
	         catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		String f;
		try {
			for(int i=0;i<array.length();i++)
			{
				a = array.get(i);
				f=a.toString();
				//Formatear el String que nos devuelve
				
				int inicio = f.indexOf("[");
				int fin = f.indexOf("]");
				String sinCorchete=f.substring(inicio + 1, fin);
				
				
				
				 String [ ] textElements = sinCorchete.split (","); //separo por coma
				 int inicio2= sinCorchete.indexOf('"');
				 int fin2 = sinCorchete.indexOf("T");
				 
				 String sinComilla=textElements[textElements.length-1].substring(inicio2 + 1, fin2);//ultimo dia

				 String h = "";
				 
				    SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
				    SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

				    try {

				      h = myFormat.format(fromUser.parse(sinComilla));
				    } catch (ParseException e) {
				        e.printStackTrace();
				    }
				 
				 
				    String patron = "dd/MM/yyyy";
					SimpleDateFormat formato = new SimpleDateFormat(patron);		    
				    String truco=formato.format(new Date());
					
				   
					
					if(h.contains(truco))
						juntos.setText("Hoy, d�a "+h);
					else
						juntos.setText("El d�a "+h);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	        //Consulta para ver el perfil del usuario seleccionado en la tabla
	        pepe=GET("http://54.72.215.193/api/metodos/perfil/?mac="+macDestino);
	      
	        try {
	              json = new JSONObject(pepe);
	          } catch (JSONException e) {
	              e.printStackTrace();
	          }
	        
	          if(json!=null)
	          {
		          try {
		           nom=json.getString("name");		          
		           est=json.getJSONObject("profile").getString("estado");
		           cont=json.getJSONObject("profile").getString("megusta");
		          imagen=json.getJSONObject("profile").getString("foto");
		             
		          } catch (JSONException e) {
		              e.printStackTrace();
		          }	          
		          nombre.setText(nom.replaceAll("--"," "));
		          estado.setText(est.replaceAll("--"," "));
		          contador.setText(cont);
		          
		          
		          if(!imagen.equals("nada"))	   	   		 
		        	  im.setImageBitmap(  redimensionarImagenMaximo(StringToBitMap2(imagen), 150, 150));	  	   		 
		   	   	 else
		   	   		 
		   	   			 im.setImageResource(R.drawable.usuario);    
	          }
	          
	          
	        //Boton megusta
	  		mg.setOnClickListener(new OnClickListener()
	  		{
	  			
	  			
	  			
	  			
	  			String request;
	  			String comprobar;
	  			JSONObject json = null;
		        String nom = null,est=null,cont=null;
		        JSONArray getJSONArray = null;
	  			
	  			boolean t=true;
	  	        @SuppressLint("NewApi")
				public void onClick(View v)
	  	           {
	  	        	//Comprobamos que no se haya hecho un me gusta antes de 24 horas
	  	        	comprobar=GET("http://54.72.215.193/api/metodos/comprobarDiario/?mac_source="+macSource+"&mac_target="+macDestino);
	  	        	if(!comprobar.startsWith("t"))
	  	        	
	  	        	{
	  	        		request=GET("http://54.72.215.193/api/metodos/megusta/?mac_target="+macDestino+"&mac_source="+macSource);
	  	        	}
	  	        	else
	  	        	{
	  	        		mg.setActivated(false);
	  	        		Toast.makeText(Perfil.this, "No han pasado 24 h desde el anterior Me gusta", Toast.LENGTH_LONG).show();
	  	        	}
	  	           
			  	      String result="";  
			  	      result=GET("http://54.72.215.193/api/metodos/perfil/?mac="+macDestino);
				      
				        try {
				              json = new JSONObject(result);
				          } catch (JSONException e) {
				              e.printStackTrace();
				          }
				        
				          if(json!=null)
				          {
					          try {
					           
					           cont=json.getJSONObject("profile").getString("megusta");
					             
					          } catch (JSONException e) {
					              e.printStackTrace();
					          }	          
					          contador.setText(cont); //Actualizo el contador de megusta   
					         
				          }
	  	           }
	  			
	  		
	 	 });
	  		
	  		
	  		 //Boton mas dias que estuvimos juntos
	  		masdias.setOnClickListener(new OnClickListener()
	  		{
	  			
	  	        public void onClick(View v)
	  	           {
	  	        	
	  	        	
	  	        	Intent in = new Intent(getApplicationContext(), DiasjuntosActivity.class);
	            	   in.putExtra("fecha",a.toString());
	            	   
	            	   
	            	   startActivity(in); 
			  	     
	  	           }
	  		
	 	 });
	  		
	  
	  		 //Boton Bloquear
	  		blo.setOnClickListener(new OnClickListener()
	  		{
	  			
	  	        public void onClick(View v)
	  	           {
	  	        	
	  	        		GET("http://54.72.215.193/api/metodos/bloquear/?mac_source="+macSource+"&mac_target="+macDestino);
	  	        		Toast.makeText(Perfil.this, "Usuario bloqueado", Toast.LENGTH_LONG).show();
	  	        	
	  	           
			  	     
	  	           }
	  		
	 	 });
   		 }
        }
   		 
    
        
        
     
        public static String GET(String url){
            InputStream inputStream = null;
            String result = "";
            try {

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return result;
        }
        private static String convertInputStreamToString(InputStream inputStream) throws IOException{
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
        
        @Override
 	   public boolean onCreateOptionsMenu(Menu menu) {
 	       MenuInflater inflater = getMenuInflater();
 	       inflater.inflate(R.menu.menu_propiedades, menu);
 	       return true;
 	   }
 	
 	@Override
 	   public boolean onOptionsItemSelected(MenuItem item) {
 	      switch (item.getItemId()) {
 	      case R.id.perfil:
 	    	 if (!verificaConexion(this)) {
				    Toast.makeText(getBaseContext(),
				            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
	    	  }
	    	  else
	    	  {
 	    		Intent in = new Intent(Perfil.this,EditarperfilActivity .class);
              	startActivity(in);
	    	  }
 	         return true;
 	         
 	      case R.id.bloqueados:
 	    	 if (!verificaConexion(this)) {
				    Toast.makeText(getBaseContext(),
				            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
	    	  }
	    	  else
	    	  {
 	    		Intent in2 = new Intent(Perfil.this,ListadobloqueadosActivity .class);
              	startActivity(in2);
	    	  }
 		         return true;
 	 
 	      case R.id.salir:
 	    	 finish();              
             System.runFinalization();
             System.exit(0);
              
 	         return true;
 	 
 	      default:
 	         return super.onOptionsItemSelected(item);
 	      }
 	   }
 	
 	 public Bitmap StringToBitMap2(String encodedString){
	     try{
	    	 BitmapFactory.Options options = new BitmapFactory.Options();	
		       options.inSampleSize = 0;
	    	 
	       byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
	       Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
	       return bitmap;
	     }catch(Exception e){
	       e.getMessage();
	       return null;
	     }
	      }
  
 	 
 	 
 	public Bitmap redimensionarImagenMaximo(Bitmap mBitmap, float newWidth, float newHeigth){
		   //Redimensionamos
		   int width = mBitmap.getWidth();
		   int height = mBitmap.getHeight();
		   float scaleWidth = ((float) newWidth) / width;
		   float scaleHeight = ((float) newHeigth) / height;
		   // create a matrix for the manipulation
		   Matrix matrix = new Matrix();
		   // resize the bit map
		   matrix.postScale(scaleWidth, scaleHeight);
		   // recreate the new Bitmap
		   return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
		}
 	
 	 public boolean verificaConexion(Context ctx) {
		    boolean bConectado = false;
		    ConnectivityManager connec = (ConnectivityManager) ctx
		            .getSystemService(Context.CONNECTIVITY_SERVICE);
		    // No s�lo wifi, tambi�n GPRS
		    NetworkInfo[] redes = connec.getAllNetworkInfo();
		    
		    for (int i = 0; i < 2; i++) {
		        // �Tenemos conexi�n? ponemos a true
		        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
		            bConectado = true;
		        }
		    }
		    return bConectado;
		}
 	 
        
}
