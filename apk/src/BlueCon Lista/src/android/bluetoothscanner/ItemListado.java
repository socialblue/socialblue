package android.bluetoothscanner;

import java.util.ArrayList;

import android.widget.ArrayAdapter;

public class ItemListado extends ArrayList<ItemListadoAdapter> {
	protected long id;
	protected String rutaImagen;
	protected String nombre;
	protected String tipo;
    protected String contador;
    protected String mac;
	
	
	public ItemListado() {
		this.nombre = "";
		this.tipo = "";
		this.rutaImagen = "";
        this.contador = "";
        this.mac="";
	}
	
	public ItemListado(long id, String nombre, String tipo, String contador) {
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.rutaImagen = "";
        this.contador = contador;
        this.mac = mac;
	}
	
	public ItemListado(long id, String nombre, String tipo, String rutaImagen, String contador) {
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.rutaImagen = rutaImagen;
        this.contador = contador;
       
	}
	
	public ItemListado(long id, String nombre, String tipo, String rutaImagen, String contador,String mac) {
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.rutaImagen = rutaImagen;
        this.contador = contador;
        this.mac=mac;
	}
	
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getRutaImagen() {
		return rutaImagen;
	}
	
	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

    public String getcontador() {
        return contador;
    }

    public void setcontador(String contador) {
        this.contador = contador;
    }
}
