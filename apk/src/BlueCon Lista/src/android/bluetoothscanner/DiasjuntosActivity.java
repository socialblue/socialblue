package android.bluetoothscanner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class DiasjuntosActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.diasjuntos);
		
		
		
		
		
		
		if (!verificaConexion(getBaseContext())) {
			    Toast.makeText(getBaseContext(),
			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
			    Intent inten2 = new Intent(DiasjuntosActivity.this,Perfil.class);
	            startActivity(inten2);
	            finish();
			   
			}
		else
		{

		String f;
		
		TextView eti= (TextView) findViewById(R.id.eticerca);
		TextView fechas= (TextView) findViewById(R.id.labelfechas);
		
		 Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
			eti.setTypeface(font);
			fechas.setTypeface(font);
		
        Intent in = getIntent();
        final String TAG_FECHAS = "fecha";
		
        f=in.getStringExtra(TAG_FECHAS);
        

        int inicio = f.indexOf("[");
		int fin = f.indexOf("]");

		String sinCorchete=f.substring(inicio + 1, fin);
		
		String [ ] textElements = sinCorchete.split (","); //Aqui esta separado or coma
		
		
		 int inicio2= sinCorchete.indexOf('"');
		 int fin2 = sinCorchete.indexOf("T");
		 
		 String sinComilla = "";
		 String h = "";
		 SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
		 SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
		 String patron = "dd/MM/yyyy";
		 SimpleDateFormat formato = new SimpleDateFormat(patron);

		for(int i=0;i<textElements.length;i++)
		{
				
			    try {

			    	
			    	
			    	
			    	
			      h =h+"\n"+ myFormat.format(fromUser.parse(textElements[i].substring(inicio2 + 1, fin2)));
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }
					   
		}

		 
		  
		 
		   String truco=formato.format(new Date());
		
		if(h.contains(truco))
			fechas.setText("Hoy"+h);
		else
			fechas.setText("Hoy no"+h);
		

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.diasjuntos, menu);
		return true;
	}
	
	//c�digo para cada opci�n de men�
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) 
        {
            case R.id.miperfil:           	
            	Intent in = new Intent(getApplicationContext(),EditarperfilActivity .class);           	
            	startActivity(in);     
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    public boolean verificaConexion(Context ctx) {
	    boolean bConectado = false;
	    ConnectivityManager connec = (ConnectivityManager) ctx
	            .getSystemService(Context.CONNECTIVITY_SERVICE);
	    // No s�lo wifi, tambi�n GPRS
	    NetworkInfo[] redes = connec.getAllNetworkInfo();
	    
	    for (int i = 0; i < 2; i++) {
	        // �Tenemos conexi�n? ponemos a true
	        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
	            bConectado = true;
	        }
	    }
	    return bConectado;
	}

}
