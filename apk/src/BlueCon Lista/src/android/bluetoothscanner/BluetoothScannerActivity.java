package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.widget.AdapterView.OnItemClickListener;






import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class BluetoothScannerActivity extends Activity {

    ArrayList<ItemListado> itemsCompra=new ArrayList<ItemListado>();
    String pepe="";
    String contador="";		
	private ItemListadoAdapter btArrayAdapter;
	final BluetoothAdapter myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();

 
 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
      
        setContentView(R.layout.activity_main);
        
        
        
        if (!verificaConexion(getBaseContext())) {
		    Toast.makeText(getBaseContext(),
		            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
		    
		}
        
        

        myBlueToothAdapter.startDiscovery();
    	Toast.makeText(BluetoothScannerActivity.this, "Buscando personas...", Toast.LENGTH_SHORT).show();

        final Button scanb = (Button)findViewById(R.id.button);
        final ListView Deviceslist = (ListView)findViewById(R.id.listView12);   
        Deviceslist.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        
        Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
		scanb.setTypeface(font);
        
        btArrayAdapter = new ItemListadoAdapter(BluetoothScannerActivity.this,itemsCompra);    
        Deviceslist.setAdapter(btArrayAdapter);


        //Turn on Bluetooth
        if (myBlueToothAdapter==null)
         Toast.makeText(BluetoothScannerActivity.this, "Tu telefono no soporta Bluetooth", Toast.LENGTH_LONG).show();
        else if (!myBlueToothAdapter.isEnabled()) {

        	Intent BtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        	
            startActivityForResult(BtIntent, 0);
            //Toast.makeText(BluetoothScannerActivity.this, "Activando Bluetooth", Toast.LENGTH_LONG).show();
            if(BluetoothAdapter.STATE_ON==12)
        	{
        		Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        		discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 2500);       		
          	
          	startActivity(discoverableIntent);
        	}
          
        }
        
        
        
        //Boton de scaneo
        scanb.setOnClickListener(new OnClickListener()
        {
         public void onClick(View v)
            {
        	 if (!verificaConexion(getBaseContext())) {
     		    Toast.makeText(getBaseContext(),
     		            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
     		    
     		}
        	 else
        	 {
        	 
        	 
        	 
        	btArrayAdapter.clear();
        	btArrayAdapter.notifyDataSetChanged();
        	Deviceslist.getEmptyView();
        	myBlueToothAdapter.startDiscovery();
        	 
        	
        	Toast.makeText(BluetoothScannerActivity.this, "Buscando personas...", Toast.LENGTH_LONG).show();

             }
            }
         
        });
        
        
        //Elementos de la lista
        Deviceslist.setOnItemClickListener(new OnItemClickListener() 
        {
        	public void onItemClick(AdapterView<?> parent, View view,int position, long id) 
        	{
        		 if (!verificaConexion(getBaseContext())) {
        			    Toast.makeText(getBaseContext(),
        			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
        			    
        			}else{
        		
        		String macSource=myBlueToothAdapter.getAddress();
        		String mac=null;
        		
               ItemListado a=(ItemListado) parent.getItemAtPosition(position);
                mac=a.getMac();

               //Si son usuarios registrados en el sistema vamos a la siguiente pantalla pasandole la mac, para posteriormente realizar la consulta
               if(mac!="000000") 
               {
            	   Intent in = new Intent(getApplicationContext(), Perfil.class);
            	   in.putExtra("mac",mac);
            	   in.putExtra("macSource",macSource);
            	   
            	   startActivity(in);
             
               }  
               else
               {
            	   Toast.makeText(BluetoothScannerActivity.this, "No pertnece a Blue", Toast.LENGTH_LONG).show();
               }
             }
        	}
      
		});
	
        registerReceiver(FoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));    
    }
    
   
    public boolean verificaConexion(Context ctx) {
	    boolean bConectado = false;
	    ConnectivityManager connec = (ConnectivityManager) ctx
	            .getSystemService(Context.CONNECTIVITY_SERVICE);
	    // No s�lo wifi, tambi�n GPRS
	    NetworkInfo[] redes = connec.getAllNetworkInfo();
	    // este bucle deber�a no ser tan �apa
	    for (int i = 0; i < 2; i++) {
	        // �Tenemos conexi�n? ponemos a true
	        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
	            bConectado = true;
	        }
	    }
	    return bConectado;
	}

    
    
    @Override
    protected void onDestroy() { 
    // TODO Auto-generated method stub 
    super.onDestroy();
    Intent data = new Intent();
    data.setData(Uri.parse("hola"));
    setResult(RESULT_OK, data);
    finish();
    unregisterReceiver(FoundReceiver);
    }
   
    private final BroadcastReceiver FoundReceiver = new BroadcastReceiver()
    { 
     @Override
    
     public void onReceive(Context context, Intent intent) {
      // TODO Auto-generated method stub 

         String result="";
         String result1="";
    	 String action = intent.getAction();  
     
      if(BluetoothDevice.ACTION_FOUND.equals(action)) 
      {
    	  BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
    	  pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+device.getAddress());

          String estado="";
          
          String nombre="";
          JSONObject json = null;
          String contadorDiario="";

          try {
              json = new JSONObject(pepe);
          } catch (JSONException e) {
              e.printStackTrace();
          }

    	ArrayList<ItemListado> items = new ArrayList<ItemListado>();
    	
    	
          JSONArray getJSONArray = null;
          if(json!=null){
          try {
           nombre=json.getString("name").replaceAll("--"," ");
          //getJSONArray=json.getJSONArray("profile");
             estado=json.getJSONObject("profile").getString("estado").replaceAll("--"," ");
           contador=json.getJSONObject("profile").getString("megusta");
           contadorDiario=json.getJSONObject("profile").getString("megustadiario");
             // estado=json.optString("profile.estado");
          } catch (JSONException e) {
              e.printStackTrace();
          }

}
          if(json!=null)
          {
        	  result=GET("http://54.72.215.193/api/metodos/bloqueado/?mac_source="+myBlueToothAdapter.getAddress()+"&mac_target="+device.getAddress());
        	  result1=GET("http://54.72.215.193/api/metodos/bloqueado/?mac_source="+device.getAddress()+"&mac_target="+myBlueToothAdapter.getAddress());
        	  if(!result.startsWith("t") && !result1.startsWith("t"))             
        	  {
        		  //si no pone foto a la hora del registro
            	  itemsCompra.add(new ItemListado(1, nombre,estado, "drawable/usuario",contadorDiario+"/"+contador,device.getAddress()));
            	  
            	  //Estuvimos juntos...
            	  String result2;
        	      result2=GET("http://54.72.215.193/api/metodos/visto/?mac_source="+myBlueToothAdapter.getAddress()+"mac_target="+device.getAddress());
        	  
        	  }
        	  else
              {
        		  if(result.startsWith("t"))
            	  Toast.makeText(BluetoothScannerActivity.this, "Hay usuarios bloqueados cerca", Toast.LENGTH_LONG).show();
              }

          }
          else //aparece El nombre, el estado y el contador de megustas
          {
        	  itemsCompra.add(new ItemListado(0, device.getName(),device.getAddress(), "drawable/g","0","000000"));
        	  //itemsCompra.add(new ItemListado(1, device.getName(), device.getAddress(), "drawable/anonimoms","0"));
        	  
          }
    	

    	 btArrayAdapter.notifyDataSetChanged();
      
      }
  
      
  }};
     
  
  
  public void onResume(Context context,Intent intent)
  {
	  super.onResume();
	  
	  
	  
	
	  BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	  String pepe2=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+device.getAddress());
	 
      String estado="";
      
      String nombre="";
      JSONObject json = null;
      String contadorDiario="";

      try {
          json = new JSONObject(pepe2);
      } catch (JSONException e) {
          e.printStackTrace();
      }

	ArrayList<ItemListado> items = new ArrayList<ItemListado>();
	
	
      JSONArray getJSONArray = null;
      if(json!=null){
      try {
       nombre=json.getString("name").replaceAll("--"," ");
      //getJSONArray=json.getJSONArray("profile");
         estado=json.getJSONObject("profile").getString("estado").replaceAll("--"," ");
       contador=json.getJSONObject("profile").getString("megusta");
       contadorDiario=json.getJSONObject("profile").getString("megustadiario");
         // estado=json.optString("profile.estado");
      } catch (JSONException e) {
          e.printStackTrace();
      }

}
     
      //aparece El nombre, el estado y el contador de megustas
      
    	  String result=GET("http://54.72.215.193/api/metodos/bloqueado/?mac_source="+myBlueToothAdapter.getAddress()+"&mac_target="+device.getAddress());
    	  String result1=GET("http://54.72.215.193/api/metodos/bloqueado/?mac_source="+device.getAddress()+"&mac_target="+myBlueToothAdapter.getAddress());
    	  
    	  if(!result.startsWith("t") && !result1.startsWith("t"))
    	  {

        	  itemsCompra.add(new ItemListado(1, nombre,estado, "drawable/anonimo",contadorDiario+"/"+contador,device.getAddress()));
        	  
        	  //Estuvimos juntos...
        	  String result2;
    	      result2=GET("http://54.72.215.193/api/metodos/visto/?mac_source="+myBlueToothAdapter.getAddress()+"mac_target="+device.getAddress());
    	  
    	  }
    	  else
          {
    		  if(result.startsWith("t"))
        	  Toast.makeText(BluetoothScannerActivity.this, "Hay usuarios bloqueados cerca", Toast.LENGTH_LONG).show();
          }
      
	

	 btArrayAdapter.notifyDataSetChanged();
	  
	  
  		
  	
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.menu_propiedades, menu);
      return true;
  }

@Override
  public boolean onOptionsItemSelected(MenuItem item) {
     switch (item.getItemId()) {
     case R.id.perfil:
    	 if (!verificaConexion(this)) {
			    Toast.makeText(getBaseContext(),
			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
 	  }
 	  else
 	  {
   		Intent in = new Intent(BluetoothScannerActivity.this,EditarperfilActivity .class);
        	startActivity(in);
 	  }
        return true;
        
     case R.id.bloqueados:
    	 if (!verificaConexion(this)) {
			    Toast.makeText(getBaseContext(),
			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
 	  }
 	  else
 	  {
   		Intent in2 = new Intent(BluetoothScannerActivity.this,ListadobloqueadosActivity .class);
        	startActivity(in2);
 	  }
        	return true;

     case R.id.salir:
    	 finish();              
         System.runFinalization();
         System.exit(0);
          
        return true;

     default:
        return super.onOptionsItemSelected(item);
     }
  }


  
  public static String GET(String url){
      InputStream inputStream = null;
      String result = "";
      try {

          // create HttpClient
          HttpClient httpclient = new DefaultHttpClient();
          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
          // make GET request to the given URL
          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

          // receive response as inputStream
          inputStream = httpResponse.getEntity().getContent();

          // convert inputstream to string
          if(inputStream != null)
              result = convertInputStreamToString(inputStream);
          else
              result = "Did not work!";

      } catch (Exception e) {
          Log.d("InputStream", e.getLocalizedMessage());
      }

      return result;
  }
  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
      String line = "";
      String result = "";
      while((line = bufferedReader.readLine()) != null)
          result += line;

      inputStream.close();
      return result;

  }

}

       