package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListadomegustaActivity extends Activity {
	
	
	private static final int EDITAR= Menu.FIRST; 
	private static final int PASS= Menu.FIRST+1; 
	private static final int DES= Menu.FIRST+2; 
	
	ArrayList<ItemListado> itemsCompra;
	JSONArray getJSONArray = null;
	JSONArray array = null;
	ListView lista;
	BluetoothAdapter myBlueToothAdapter;
	String mac;
	
	ProgressBar progressBar;
	TextView loadText;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.listadomegusta);
		

		if (!verificaConexion(this)) {
		    Toast.makeText(getBaseContext(),"Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();	    
		}
		else
		{
		
		
		final Button buscar = (Button)findViewById(R.id.button1);
		
		Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
		buscar.setTypeface(font);
		
		
		lista=(ListView) findViewById(R.id.listView1);
		
		lista.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
		itemsCompra=new ArrayList<ItemListado>();
	    String pepe="";
	    ItemListadoAdapter btArrayAdapter;
		
		btArrayAdapter = new ItemListadoAdapter(ListadomegustaActivity.this,itemsCompra);   
		
		btArrayAdapter.clear();
		 
		//Asociamos los men�s contextuales a los controles
	    registerForContextMenu(lista);
	    lista.setAdapter(btArrayAdapter);
		
   	  	 pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+myBlueToothAdapter.getAddress());

         String estado="";
         String contador="";
         String nombre="";
         JSONObject json = null;
         String contadorDiario="";

         try {
             json = new JSONObject(pepe);
         } catch (JSONException e) {
             e.printStackTrace();
         }

   	ArrayList<ItemListado> items = new ArrayList<ItemListado>();
         if(json!=null){
 
         try {
		array = json.getJSONArray(("Megusta"));

	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	Object a = null;
	try {
		for(int i=0;i<array.length();i++)
		{
			a = array.get(i);
			pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+a);
			 try {
	             json = new JSONObject(pepe);
	         } catch (JSONException e) {
	             e.printStackTrace();
	         }
			nombre=json.getString("name").replaceAll("--"," ");
	          //getJSONArray=json.getJSONArray("profile");
	             estado=json.getJSONObject("profile").getString("estado").replaceAll("--"," ");
	           contador=json.getJSONObject("profile").getString("megusta");
	           contadorDiario=json.getJSONObject("profile").getString("megustadiario");
	           
	           
	           
	           
	           
	           itemsCompra.add(new ItemListado(1, nombre,estado, a.toString(),contadorDiario+"/"+contador,a.toString()));

		}
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
         }


         lista.setOnItemClickListener(new OnItemClickListener() 
	        {
	        	public void onItemClick(AdapterView<?> parent, View view,int position, long id) 
	        	{
	        		
	        		 if (!verificaConexion(getBaseContext())) {
	        			    Toast.makeText(getBaseContext(),
	        			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();        			    
	        			}
	        		 else
	        		 {	
			               ItemListado a=(ItemListado) parent.getItemAtPosition(position);
			                mac=a.getMac();
		
			               //Si son usuarios registrados en el sistema vamos a la siguiente pantalla pasandole la mac, para posteriormente realizar la consulta
			               if(mac!=null) 
			               {
			            	   Intent in = new Intent(getApplicationContext(), Perfil.class);
			            	   in.putExtra("mac",mac);
			            	   startActivity(in);
			               }
	             }
	        	}
			});
		
		//Boton de escanear
		buscar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {	
				if (!verificaConexion(getBaseContext())) 
				{
				    Toast.makeText(getBaseContext(), "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
				}
				else
				{
	            	Intent inten2 = new Intent(ListadomegustaActivity.this,BluetoothScannerActivity.class);
	           	 	startActivity(inten2);	
				}
			}
		});		
	}
	}

	@Override
	   public boolean onCreateOptionsMenu(Menu menu) {
	       MenuInflater inflater = getMenuInflater();
	       inflater.inflate(R.menu.menu_propiedades, menu);
	       return true;
	   }
	
	@Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      switch (item.getItemId()) {
	      case R.id.perfil:
	    	  
	    	  if (!verificaConexion(this)) {
				    Toast.makeText(getBaseContext(),
				            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
	    	  }
	    	  else
	    	  {
	    		Intent in = new Intent(ListadomegustaActivity.this,EditarperfilActivity .class);
             	startActivity(in);
	    	  }		
	         return true;
	    	  
	         
	      case R.id.bloqueados:
	    		
	    	  if (!verificaConexion(this)) {
				    Toast.makeText(getBaseContext(),
				            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
	    	  }
	    	  else
	    	  {
	    	  Intent in2 = new Intent(ListadomegustaActivity.this,ListadobloqueadosActivity .class);
             	startActivity(in2);
	    	  }
             	
		         return true;
	 
	      case R.id.salir:
	    	  finish();              
              System.runFinalization();
              System.exit(0);
               
             
	         return true;
	 
	      default:
	         return super.onOptionsItemSelected(item);
	      }
	   }
	

	
	    //Este evento se ejecuta despues de venir de la siguiente pantalla para refrecar la tabla
	    
	    public void onResume(){
		    super.onResume();

			    String pepe="";
			    ItemListadoAdapter btArrayAdapter;
			   
				btArrayAdapter = new ItemListadoAdapter(ListadomegustaActivity.this,itemsCompra);   
				btArrayAdapter.clear();
				
				 
				//Asociamos los men�s contextuales a los controles
			    registerForContextMenu(lista);
			    lista.setAdapter(btArrayAdapter);
				
		   	  	 pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+myBlueToothAdapter.getAddress());

		         String estado="";
		         String contador="";
		         String nombre="";
		         JSONObject json = null;
		         String contadorDiario="";

		         try {
		             json = new JSONObject(pepe);
		         } catch (JSONException e) {
		             e.printStackTrace();
		         }

		   	ArrayList<ItemListado> items = new ArrayList<ItemListado>();
		         if(json!=null){
		 
		         try {
				array = json.getJSONArray(("Megusta"));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Object a = null;
			try {
				for(int i=0;i<array.length();i++)
				{
					a = array.get(i);
					pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+a);
					 try {
			             json = new JSONObject(pepe);
			         } catch (JSONException e) {
			             e.printStackTrace();
			         }
					nombre=json.getString("name").replaceAll("--"," ");
			          //getJSONArray=json.getJSONArray("profile");
			             estado=json.getJSONObject("profile").getString("estado").replaceAll("--"," ");
			           contador=json.getJSONObject("profile").getString("megusta");
			           contadorDiario=json.getJSONObject("profile").getString("megustadiario");
			           itemsCompra.add(new ItemListado(1, nombre,estado, "drawable/usuario",contadorDiario+"/"+contador,a.toString()));

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		         }

	    }

	   
	    
	    
	    
	    //Para salir de la aplicacion preguntando primero
	    @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode == KeyEvent.KEYCODE_BACK) {
	      new AlertDialog.Builder(this)
	         .setIcon(android.R.drawable.ic_dialog_alert)
	         .setTitle("Blue")
	         .setMessage("�Seguro que desea Salir de Blue?")
	         .setNegativeButton(android.R.string.cancel, null)
	         .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
	           @Override
	           public void onClick(DialogInterface dialog, int which){
	            
	        	   android.os.Process.killProcess(android.os.Process.myPid());
                   System.exit(0);
	           	
	           }
	         })
	         .show();
	       // Si el listener devuelve true, significa que el evento esta procesado, y nadie debe hacer nada mas
	       return true;
	     }
	    //para las demas cosas, se reenvia el evento al listener habitual
	     return super.onKeyDown(keyCode, event);
	   
	    }
	  //Para hacer las llamadas a la api
		 public static String GET(String url){
		        InputStream inputStream = null;
		        String result = "";
		        try {

		            // create HttpClient
		            HttpClient httpclient = new DefaultHttpClient();
		            httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
		            // make GET request to the given URL
		            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

		            // receive response as inputStream
		            inputStream = httpResponse.getEntity().getContent();

		            // convert inputstream to string
		            if(inputStream != null)
		                result = convertInputStreamToString(inputStream);
		            else
		                result = "Did not work!";

		        } catch (Exception e) {
		            Log.d("InputStream", e.getLocalizedMessage());
		        }

		        return result;
		    }
		    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
		        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		        String line = "";
		        String result = "";
		        while((line = bufferedReader.readLine()) != null)
		            result += line;

		        inputStream.close();
		        return result;
		    }
		    
		    public boolean verificaConexion(Context ctx) {
			    boolean bConectado = false;
			    ConnectivityManager connec = (ConnectivityManager) ctx
			            .getSystemService(Context.CONNECTIVITY_SERVICE);
			    // No s�lo wifi, tambi�n GPRS
			    NetworkInfo[] redes = connec.getAllNetworkInfo();
			    
			    for (int i = 0; i < 2; i++) {
			        // �Tenemos conexi�n? ponemos a true
			        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
			            bConectado = true;
			        }
			    }
			    return bConectado;
			}

}
