package android.bluetoothscanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;


import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.Layout;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

//import org.apache.commons.codec.binary.Base64;

import javax.xml.*;

public class Registro extends Activity {

	
	
	private int SELECT_PICTURE= 237487;
	private int TAKE_PICTURE = 829038;
	
	
	String  name = Environment.getExternalStorageDirectory() + "/test.jpg";
	 Uri currImageURI;
	
	 Button server,ver;
	 EditText imagen;
	 ImageView ima, ima2,subir ; 
	 ImageView subir2;
	 Bitmap bitmap;
	 Bitmap bitmapNuevo;
	 Intent intent; 
	 int code;
	
	 String g="nada",im;
	 String dd ;
	
	
	
	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	String mac= mBluetoothAdapter.getAddress();
	 
	 
		
	 
	@Override
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_registro);
		
		
		if (!verificaConexion(this)) {
		    Toast.makeText(getBaseContext(),
		            "Necesitas conexi�n a Internet.", Toast.LENGTH_SHORT).show();
		    Intent inten2 = new Intent(Registro.this,Blue.class);
            startActivity(inten2);
            finish();
		   
		}
	
		
		//Boton
		final Button reg = (Button)findViewById(R.id.btnRegistro);
		  subir = (ImageView)findViewById(R.id.subir);
		
		
		  final TextView eti=(TextView) findViewById(R.id.textView5);
		
		final EditText nom=(EditText) findViewById(R.id.nombre);
		final EditText ema=(EditText) findViewById(R.id.email);
		final EditText esta=(EditText) findViewById(R.id.estado);
		final EditText tel=(EditText) findViewById(R.id.telefono);
		final EditText contra=(EditText) findViewById(R.id.contrasena);
		final EditText contra2=(EditText) findViewById(R.id.contrasena2);
		

		Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
		nom.setTypeface(font);
		ema.setTypeface(font);
		tel.setTypeface(font);
		contra.setTypeface(font);
		contra2.setTypeface(font);
		esta.setTypeface(font);
		reg.setTypeface(font);
		eti.setTypeface(font);
	
		nom.requestFocus();
		
		nom.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				nom.setText("");
				return false;
			}
		});
		ema.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				ema.setText("");
				return false;
			}
		});	
		contra.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						contra.setText("");
						return false;
					}
				});
		contra2.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				contra2.setText("");
				return false;
			}
		});
		tel.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				tel.setText("");
				return false;
			}
		});
					
				
		esta.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				esta.setText("");
				return false;
			}
		});
		String email, telefono, estado;
		
				//Boton de elegir la imagen
				subir.setOnClickListener(new OnClickListener() {   
					 public void onClick(View v) {

						 intent =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
			       		 code = TAKE_PICTURE;
			       		 
			       		
			       		 final CharSequence[] items = {"Seleccionar de la galer�a"};

			      		  AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
			      		  builder.setTitle("Seleccionar una foto");
			      		  builder.setItems(items, new DialogInterface.OnClickListener() {
			      		    public void onClick(DialogInterface dialog, int item) {
			      		      switch(item)
			      		      {
			      		       case 0:
			      		    
			      		    	intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
			      				code = SELECT_PICTURE;
			      				startActivityForResult(intent, code);
			      		     break;
			      		       case 1: //camara
			      		    	  
			      		    	   
//			      		    	 //Creamos el Intent para llamar a la Camara
//			      		         Intent cameraIntent = new Intent(
//			      		            android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
//			      		         //Creamos una carpeta en la memeria del terminal
//			      		         File imagesFolder = new File(
//			      		            Environment.getExternalStorageDirectory(), "Tutorialeshtml5");
//			      		         imagesFolder.mkdirs();   
//			      		         //a�adimos el nombre de la imagen
//			      		         File image = new File(imagesFolder, "foto.jpg"); 
//			      		         Uri uriSavedImage = Uri.fromFile(image);
//			      		         //Le decimos al Intent que queremos grabar la imagen
//			      		         cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
//			      		         //Lanzamos la aplicacion de la camara con retorno (forResult)
//			      		         startActivityForResult(cameraIntent, code);
			      		    	   
			      		    	   
	
			      		    	   
			      		    	//Uri output = Uri.fromFile(new File(name));
			      		    	
			      		    	/*Bueno   
			      		    	 
			      		    	   Uri output = Uri.fromFile(new File(name));
				      		    	intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
				      		    	startActivityForResult(intent, code);
			      		    	   
			      		    	 */  
			      		    	   
			      		    	   
			      		    	   
//			      		    	startActivityForResult(new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE), TAKE_PICTURE);  
//			      		    	   
//			      		    	   
//			      		    	Uri output = Uri.fromFile(new File(name));
//			      		    	intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
//			      		    	startActivityForResult(intent, code);
			      		     break;
			      		      }
			      		            
			      		    }
			      		  });
			      		  AlertDialog alert = builder.create();
			      		  alert.show(); 
			      		     
			      		//startActivityForResult(intent, code);
			   		 		
			       			
					 }
					}); 
				
				
		
		
		
		
		//Boton de registro
		 reg.setOnClickListener(new OnClickListener()
	        {

			 
	         	@SuppressLint("NewApi")
				public void onClick(View v)
	            {

	         	
	         	String nombre=nom.getText().toString().replaceAll(" ","--");
	         	String email=ema.getText().toString().replaceAll(" ","--");
	         	String estado=esta.getText().toString().replaceAll(" ","--");
	         	String telefono=tel.getText().toString().replaceAll(" ","--");
	         	String contrasena=contra.getText().toString().replaceAll(" ","--");
	         	String contrasena2=contra2.getText().toString().replaceAll(" ","--");
	         	boolean pasa=true;
	         	
	         	
	         	
	         	//Compruebo si ya esta mac esta registrada para que no se vuelva a registrar
	       	    String pepe2=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+mac);
	         	
	       	    if(pepe2.startsWith("{"))
	         	{
	         		
	         		 Toast.makeText(Registro.this, "Este tel�fono ya est� registrado,inserte email y password", Toast.LENGTH_LONG).show();
	         		 Intent inten2 = new Intent(Registro.this,Blue.class);
                     startActivity(inten2);
                     finish();
	         	
	         	
	         	}
	       	    else
	       	    {
	       	    	if(email.isEmpty() || email=="Email")
	       	    	{
	       	    		Toast.makeText(Registro.this, "El campo Email es obligatorio", Toast.LENGTH_LONG).show();
	       	    		pasa=false;
	       	    	}
	       	    	if(contrasena.isEmpty() || contrasena=="Contrase�a")
	       	    	{
	       	    		Toast.makeText(Registro.this, "Inserte la contrase�a", Toast.LENGTH_LONG).show();
	       	    		pasa=false;
	       	    		
	       	    	}
	       	    	
	       	    	if(telefono.length()!=9)
	       	    	{
	       	    		Toast.makeText(Registro.this, "Revise el campo telefono", Toast.LENGTH_LONG).show();
	       	    		pasa=false;
	       	    	}
	       	    	
	       	    	
	       	    	if(contrasena2.compareTo(contrasena)!=0)
	       	    	{
	       	    		Toast.makeText(Registro.this, "Las contrase�as no coinciden", Toast.LENGTH_LONG).show();
	       	    		contra.setText("");
	       	    		contra2.setText("");
	       	    		pasa=false;
	       	    	}
	       	    	else if(pasa)
	       	    	{
	       	    		Toast.makeText(Registro.this, "Bienvenido a Social Blue", Toast.LENGTH_LONG).show();
				        String pepe;
			         	pepe=GET("http://54.72.215.193/api/metodos/registro?nombre="+nombre+"&email="+email+"&contrasena="+contrasena+"&estado="+estado+"&telefono="+telefono+"&foto="+g+"&mac="+mac);
			         
						if(pepe.startsWith("t"))
		                 {
		       	    		

		                      Intent inten2 = new Intent(Registro.this,Blue.class);
		                      startActivity(inten2);
		                      finish();
		
		                 }
							
			       	}
	       	    	
	       	    		
							
	       	    }
	        		
	        }
	           
	        });	 	 
	}
	    

	
	@Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	/**
    	 * Se revisa si la imagen viene de la c�mara (TAKE_PICTURE) o de la galer�a (SELECT_PICTURE)
    	 */
    	if (requestCode == TAKE_PICTURE) {
    		/**
    		 * Si se reciben datos en el intent tenemos una vista previa (thumbnail)
    		 */
    		if (data != null) {
    			/**
    			 * En el caso de una vista previa, obtenemos el extra �data� del intent y 
    			 * lo mostramos en el ImageView
    			 */
    			if (data.hasExtra("data")) { 
    				subir = (ImageView)findViewById(R.id.subir);
    				subir.setImageBitmap((Bitmap) data.getParcelableExtra("data"));
    			}
    		/**
    		 * De lo contrario es una imagen completa
    		 */    			
    		} else {
    			/**
    			 * A partir del nombre del archivo ya definido lo buscamos y creamos el bitmap
    			 * para el ImageView
    			 */
    			subir = (ImageView)findViewById(R.id.subir);
    			subir.setImageBitmap(BitmapFactory.decodeFile(name));
    			/**
    			 * Para guardar la imagen en la galer�a, utilizamos una conexi�n a un MediaScanner
    			 */
    			new MediaScannerConnectionClient() {
    				private MediaScannerConnection msc = null; {
    					msc = new MediaScannerConnection(getApplicationContext(), this); msc.connect();
    				}
    				public void onMediaScannerConnected() { 
    					msc.scanFile(name, null);
    				}
    				public void onScanCompleted(String path, Uri uri) { 
    					msc.disconnect();
    				} 
    			};				
    		}
    	/**
    	 * Recibimos el URI de la imagen y construimos un Bitmap a partir de un stream de Bytes
    	 */
    	} else if (requestCode == SELECT_PICTURE){
    		Uri selectedImage = data.getData();
    		InputStream is;
    		try {
    			
    			
    			
    			
    			
    			
    			
    			BitmapFactory.Options options = new BitmapFactory.Options();	
 		       options.inSampleSize = 8;
    		      // Bitmap bm = BitmapFactory.decodeFile(data.getDataString(), options);
    			
    		       /////
    			
    			is = getContentResolver().openInputStream(selectedImage);
    	    	BufferedInputStream bis = new BufferedInputStream(is);
    	    	bitmap = BitmapFactory.decodeStream(bis,null, options);
    	    	String j;
    	    	
    	    	
    	    	//SubirFoto nuevaTarea = new SubirFoto();
                //nuevaTarea.execute(bitmap);
    	    	
    	    	

    	    	g=BitMapToString2(bitmap);
    	    	
    	    	j=URLEncoder.encode(g, "UTF-8");		 
       		  
    	    	g=j;
    	    	
    	    	 subir.setImageBitmap(redimensionarImagenMaximo(bitmap,120,120));

 	    	final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
 			final String mac= mBluetoothAdapter.getAddress();
 	    	

 			
		      
		        	
		        	
		        	
		        
				
    		} catch (FileNotFoundException e) {} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		   

    	
    	}
	}

	
	
	public boolean verificaConexion(Context ctx) {
	    boolean bConectado = false;
	    ConnectivityManager connec = (ConnectivityManager) ctx
	            .getSystemService(Context.CONNECTIVITY_SERVICE);
	    // No s�lo wifi, tambi�n GPRS
	    NetworkInfo[] redes = connec.getAllNetworkInfo();
	    // este bucle deber�a no ser tan �apa
	    for (int i = 0; i < 2; i++) {
	        // �Tenemos conexi�n? ponemos a true
	        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
	            bConectado = true;
	        }
	    }
	    return bConectado;
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_registro, menu);
		return true;
	}
	
	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

	          // receive response as inputStream
	          inputStream = httpResponse.getEntity().getContent();

	          // convert inputstream to string
	          if(inputStream != null)
	              result = convertInputStreamToString(inputStream);
	          else
	              result = "Did not work!";

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }

	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;

	  }
	  
	  public String BitMapToString2(Bitmap bitmap){

			 ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	         bitmap.compress(Bitmap.CompressFormat.PNG,8, baos);
	         
	         byte [] b=baos.toByteArray();
	         String temp=Base64.encodeToString(b, Base64.DEFAULT);
	         return temp;
	   }
		  public Bitmap StringToBitMap2(String encodedString){
			     try{
			       byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
			       
			    
			    		   BitmapFactory.Options options = new BitmapFactory.Options();	
	 		       options.inSampleSize = 16;
	 		       
	 		      
	 		       
			       Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
			       
			       
			       return bitmap;
			     }catch(Exception e){
			       e.getMessage();
			       return null;
			     }
			      }
		 
		  public Bitmap redimensionarImagenMaximo(Bitmap mBitmap, float newWidth, float newHeigth){
			   //Redimensionamos
			   int width = mBitmap.getWidth();
			   int height = mBitmap.getHeight();
			   float scaleWidth = ((float) newWidth) / width;
			   float scaleHeight = ((float) newHeigth) / height;
			   // create a matrix for the manipulation
			   Matrix matrix = new Matrix();
			   // resize the bit map
			   matrix.postScale(scaleWidth, scaleHeight);
			   // recreate the new Bitmap
			   return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
			}
	
		  /*
		  Adrian i2f
		    
		    private String getBase64StringFromPhoto(String path)
{
File imagefile = new File(path);
       FileInputStream fis = null;
       try {
           fis = new FileInputStream(imagefile);
       } catch (FileNotFoundException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }

       //Bitmap bm = BitmapFactory.decodeStream(fis);
       BitmapFactory.Options options = new BitmapFactory.Options();	
options.inSampleSize = 8;
Bitmap bm = BitmapFactory.decodeFile(fileUri.getPath(), options);
       ByteArrayOutputStream baos = new ByteArrayOutputStream();  
       bm.compress(Bitmap.CompressFormat.PNG, 20 , baos);    
       byte[] byteArray = baos.toByteArray(); 

       String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
       String encodedString = "";
try {
encodedString = URLEncoder.encode(encodedImage.trim(), "UTF-8");
} catch (UnsupportedEncodingException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}
       return encodedString;
}
		   * 
		   * 
		   */
		  
		  
		  
		  

}
