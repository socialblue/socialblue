package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

public class ListadobloqueadosActivity extends Activity {
	
	
	private static final int DES= Menu.FIRST; 
	
	
	String macGlobal="";
	
	
	ArrayList<ItemListado> itemsCompra;
	JSONArray getJSONArray = null;
	JSONArray array = null;
	ListView lista;
	BluetoothAdapter myBlueToothAdapter;
	String mac;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.listadobloqueados);
		
		
		
		

		lista=(ListView) findViewById(R.id.listView1);
		myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
		itemsCompra=new ArrayList<ItemListado>();
	    String pepe="";
	    ItemListadoAdapter btArrayAdapter;
	    
	    TextView eti=(TextView) findViewById(R.id.etibloq);
	    
	    Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
		eti.setTypeface(font);
	    
		btArrayAdapter = new ItemListadoAdapter(ListadobloqueadosActivity.this,itemsCompra);   
		
		btArrayAdapter.clear();
		 
		//Asociamos los men�s contextuales a los controles
	    registerForContextMenu(lista);
	    lista.setAdapter(btArrayAdapter);
		
   	  	 pepe=GET("http://54.72.215.193/api/metodos/listadobloqueados/?mac_source="+myBlueToothAdapter.getAddress());

   	  	 
   	  	 
   	  	 
         String estado="";
         String contador="";
         String nombre="";
         JSONObject json = null;
         String contadorDiario="";

         try {
             json = new JSONObject(pepe);
         } catch (JSONException e) {
             e.printStackTrace();
         }
         
   	ArrayList<ItemListado> items = new ArrayList<ItemListado>();
         if(json!=null){
 
         try {
		array = json.getJSONArray(("mac_target"));

	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	Object a = null;
	try {
		for(int i=0;i<array.length();i++)
		{
			a = array.get(i);
			macGlobal=a.toString();
			
			
			pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+a);
			 try {
	             json = new JSONObject(pepe);
	         } catch (JSONException e) {
	             e.printStackTrace();
	         }
			nombre=json.getString("name").replaceAll("--"," ");
	          //getJSONArray=json.getJSONArray("profile");
	             estado=json.getJSONObject("profile").getString("estado").replaceAll("--"," ");
	           contador=json.getJSONObject("profile").getString("megusta");
	           contadorDiario=json.getJSONObject("profile").getString("megustadiario");
	           itemsCompra.add(new ItemListado(1, nombre,estado, "drawable/usuario",contadorDiario+"/"+contador,a.toString()));

		}
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
         }
		}
	
	
	
	//Para hacer las llamadas a la api
		 public static String GET(String url){
		        InputStream inputStream = null;
		        String result = "";
		        try {

		            // create HttpClient
		            HttpClient httpclient = new DefaultHttpClient();
		            httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
		            // make GET request to the given URL
		            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

		            // receive response as inputStream
		            inputStream = httpResponse.getEntity().getContent();

		            // convert inputstream to string
		            if(inputStream != null)
		                result = convertInputStreamToString(inputStream);
		            else
		                result = "Did not work!";

		        } catch (Exception e) {
		            Log.d("InputStream", e.getLocalizedMessage());
		        }

		        return result;
		    }
		    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
		        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		        String line = "";
		        String result = "";
		        while((line = bufferedReader.readLine()) != null)
		            result += line;

		        inputStream.close();
		        return result;
		    }
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listadobloqueados, menu);
		return true;
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo)
	{
	    super.onCreateContextMenu(menu, v, menuInfo);	 
	    menu.add(0, DES, 0, "Desbloquear");     
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.contextual_login, menu);
	}
	
	
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		
	    switch (item.getItemId()) {
	        case DES:
	        	//Toast.makeText(getBaseContext(),"bloqueando nuevo.... ", Toast.LENGTH_SHORT).show();
	        	String request;
	  			
	  			JSONObject json = null;
		        String nom = null,est=null,cont=null;
		        JSONArray getJSONArray = null;

		        request=GET("http://54.72.215.193/api/metodos/desbloquear/?mac_source="+myBlueToothAdapter.getAddress()+"&mac_target="+ macGlobal);    
	        	Toast.makeText(getBaseContext(), "Usuario desbloqueado", Toast.LENGTH_LONG).show();

	    		lista=(ListView) findViewById(R.id.listView1);
	    		myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
	    		itemsCompra=new ArrayList<ItemListado>();
	    	    String pepe="";
	    	    ItemListadoAdapter btArrayAdapter;
	    	    
	    	    TextView eti=(TextView) findViewById(R.id.etibloq);
	    	    
	    	    Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
	    		eti.setTypeface(font);
	    	    
	    		btArrayAdapter = new ItemListadoAdapter(ListadobloqueadosActivity.this,itemsCompra);   
	    		
	    		btArrayAdapter.clear();
	    		 
	    		//Asociamos los men�s contextuales a los controles
	    	    registerForContextMenu(lista);
	    	    lista.setAdapter(btArrayAdapter);
	    		
	       	  	 pepe=GET("http://54.72.215.193/api/metodos/listadobloqueados/?mac_source="+myBlueToothAdapter.getAddress());

	             String estado="";
	             String contador="";
	             String nombre="";
	            
	             String contadorDiario="";

	             try {
	                 json = new JSONObject(pepe);
	             } catch (JSONException e) {
	                 e.printStackTrace();
	             }
	             
	       	ArrayList<ItemListado> items = new ArrayList<ItemListado>();
	             if(json!=null){
	     
	             try {
	    		array = json.getJSONArray(("mac_target"));

	    	} catch (JSONException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}

	    	Object a = null;
	    	try {
	    		for(int i=0;i<array.length();i++)
	    		{
	    			a = array.get(i);
	    			macGlobal=a.toString();
	    			
	    			
	    			pepe=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+a);
	    			 try {
	    	             json = new JSONObject(pepe);
	    	         } catch (JSONException e) {
	    	             e.printStackTrace();
	    	         }
	    			nombre=json.getString("name").replaceAll("--"," ");
	    	          //getJSONArray=json.getJSONArray("profile");
	    	             estado=json.getJSONObject("profile").getString("estado").replaceAll("--"," ");
	    	           contador=json.getJSONObject("profile").getString("megusta");
	    	           contadorDiario=json.getJSONObject("profile").getString("megustadiario");
	    	           itemsCompra.add(new ItemListado(1, nombre,estado, "drawable/usuario",contadorDiario+"/"+contador,a.toString()));

	    		}
	    	} catch (JSONException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
	             }


	            return true;
	            
	        
	        default:
	            return super.onContextItemSelected(item);
	    
		}
	    
	}
		
	
	
	 
	 public boolean verificaConexion(Context ctx) {
		    boolean bConectado = false;
		    ConnectivityManager connec = (ConnectivityManager) ctx
		            .getSystemService(Context.CONNECTIVITY_SERVICE);
		    // No s�lo wifi, tambi�n GPRS
		    NetworkInfo[] redes = connec.getAllNetworkInfo();
		    
		    for (int i = 0; i < 2; i++) {
		        // �Tenemos conexi�n? ponemos a true
		        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
		            bConectado = true;
		        }
		    }
		    return bConectado;
		}
	
	
	

}
