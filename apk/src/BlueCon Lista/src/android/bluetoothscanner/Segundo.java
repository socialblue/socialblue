package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Segundo extends Activity {
       String macSource="";
       String macDestino="";
       
        @Override
        public void onCreate(Bundle savedInstanceState) {
        	super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_segundo);
	       
	        Intent in = getIntent();
	        final String TAG_MAC = "mac";
	        
	        TextView mac= (TextView) findViewById(R.id.mac);
	        TextView nombre = (TextView) findViewById(R.id.nombre);
	        TextView estado = (TextView) findViewById(R.id.estado);
	        TextView contador = (TextView) findViewById(R.id.contador);
	        
	        Button mg= (Button) findViewById(R.id.button1);
	        Button blo= (Button) findViewById(R.id.button2);
	        
	       String pepe;
	        // Displaying all values on the screen
	        TextView lblName = (TextView) findViewById(R.id.mac);
	        
	    	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    	macSource= mBluetoothAdapter.getAddress();
	        macDestino=in.getStringExtra(TAG_MAC);
	        lblName.setText(in.getStringExtra(TAG_MAC));
	        
	      
	        pepe=GET("http://192.168.1.102:64444/api/metodos/perfil/?mac="+in.getStringExtra(TAG_MAC));
	       
	        
	          
	        
	        JSONObject json = null;
	        
	        try {
	              json = new JSONObject(pepe);
	          } catch (JSONException e) {
	              e.printStackTrace();
	          }

	    	
	    	
	    	String nom = null,est=null,cont=null;
	          JSONArray getJSONArray = null;
	          
	          if(json!=null)
	          {
		          try {
		           nom=json.getString("name");
		          //getJSONArray=json.getJSONArray("profile");
		             est=json.getJSONObject("profile").getString("estado");
		              cont=json.getJSONObject("profile").getString("megusta");
		             // estado=json.optString("profile.estado");
		          } catch (JSONException e) {
		              e.printStackTrace();
		          }
	          
	          
	          nombre.setText(nom);
	          estado.setText(est);
	          contador.setText(cont);
	          
	          
	          
	          }
	          
	          
	          
	        //Boton megusta
	  		mg.setOnClickListener(new OnClickListener()
	  		{
	  			String request;
	  	        public void onClick(View v)
	  	           {
	  	        	 request=GET("http://192.168.1.102:64444/api/metodos/megusta/?mac_target="+macDestino+"&mac_source="+macSource);

	  	           }
	  		
	  		 });
	  		
    }
        
     
        public static String GET(String url){
            InputStream inputStream = null;
            String result = "";
            try {

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return result;
        }
        private static String convertInputStreamToString(InputStream inputStream) throws IOException{
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
}
