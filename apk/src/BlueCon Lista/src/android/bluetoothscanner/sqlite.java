package android.bluetoothscanner;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class sqlite extends SQLiteOpenHelper {

		//Sentencia SQL para crear la tabla de Usuarios
	    String sqlCreate = "CREATE TABLE mac (mac TEXT primary key, nombre TEXT)";
		
		public sqlite(Context context, String name, CursorFactory factory,int version) {
			super(context, name, factory, version);
		
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			 //Se ejecuta la sentencia SQL de creaci�n de la tabla
	        db.execSQL(sqlCreate);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			
			
			
			//eliminamos la version anterior de la tabla
	        db.execSQL("DROP TABLE IF EXISTS mac");
	 
	        //aqu� creamos la nueva versi�n de la tabla
	        db.execSQL(sqlCreate);
			
		}

	}



