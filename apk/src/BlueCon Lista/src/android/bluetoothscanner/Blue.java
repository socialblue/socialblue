package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.nsd.NsdManager.RegistrationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Blue extends Activity {

	
	private EditText email,contrasena;
	String resultado=null;
	String recor=null;
	String ema,con,ema2,con2;
	

	final BluetoothAdapter myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
	

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		
		
		final Button aceptar = (Button)findViewById(R.id.enviar);
		final Button registrate = (Button)findViewById(R.id.registrate);
		final Button recuperar = (Button)findViewById(R.id.recuperar);
		final CheckBox recordar = (CheckBox)findViewById(R.id.recordar);
		
		 email=(EditText)findViewById(R.id.email);
		 contrasena=(EditText)findViewById(R.id.contrasena);
		 
		 
		 
		 
		 
		 
		 boolean existe=false;
		
		 Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
			email.setTypeface(font);
			contrasena.setTypeface(font);
			aceptar.setTypeface(font);
			registrate.setTypeface(font);
			recuperar.setTypeface(font);
			recordar.setTypeface(font);
			
			
			
			//Compruebo si ya esta mac esta registrada para que no se vuelva a registrar
       	    String pepe2=GET("http://54.72.215.193/api/metodos/buscaramigo/?mac="+myBlueToothAdapter.getAddress());
         	
       	    if(pepe2.startsWith("{"))    	
       	    	existe=true;
       	    	
         	
       	    
			
			if(existe)
			{
       	    
			
			//Compruebo si el recordar esta a si
			
			
			 //Consulta para ver el perfil del usuario 
	        resultado=GET("http://54.72.215.193/api/metodos/perfil/?mac="+myBlueToothAdapter.getAddress());
	        JSONObject json2 = null;
	       
	        try {
	              json2 = new JSONObject(resultado);
	          } catch (JSONException e) {
	              e.printStackTrace();
	          }
	        
	          if(json2!=null)
	          {
		          try {
		           
		            recor=json2.getString("recordar");
		            ema2=json2.getJSONObject("profile").getString("email");
		            con2=json2.getJSONObject("profile").getString("contrasena");
     
		          } catch (JSONException e) {
		              e.printStackTrace();
		          }	          
		         
	          }
    		
	          //Si el usuario a elegido que no se le recuerden los datos los coge del editText
	          if(recor.equals("no"))
	          {
	        	  ema=email.getText().toString();
	        	  con=contrasena.getText().toString();
	          }
	          else if(recor.equals("si"))
	          {
	        	 ema=ema2; 
	        	 con=con2;
	        	 email.setText(ema);
	        	 contrasena.setText(con);
	        	 recordar.setChecked(true);
	        	 
	          }
			
			
			
			}

			
			//Borro lo que est� escrito y lo pongo aen blanco para que el usuario escriba
			email.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					email.setText("");
					return false;
				}
			});
			
			contrasena.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					
					
					contrasena.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
					
					
					contrasena.setText("");
					return false;
				}
			});
			
			
			
			
			
			
			
			
			
			
			
			
			
		 
		//Boton de recuperar contrase�a
		recuperar.setOnClickListener(new OnClickListener()
			{
		        public void onClick(View v)
		           {
		        		Intent inten2 = new Intent(Blue.this,RecuperarActivity.class);
		           	 	startActivity(inten2);
		           	 	finish();	
		           }		
			 });	
		//Boton de registrate, este boton pasa a la otra pantalla del registro
		registrate.setOnClickListener(new OnClickListener()
		{
	        public void onClick(View v)
	           {
	        		// Pantalla registro
	            	Intent inten2 = new Intent(Blue.this,Registro.class);
	           	 	startActivity(inten2);	           	 		
	           }	
		 });		
		aceptar.setOnClickListener(new OnClickListener()
			{
		        public void onClick(View v)
		           {
		        	
		        	
		        	
		        	
		        	
		        	boolean bConectado = false;
		    	    ConnectivityManager connec = (ConnectivityManager) v.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		    	    // No s�lo wifi, tambi�n GPRS
		    	    NetworkInfo[] redes = connec.getAllNetworkInfo();
		    	    for (int i = 0; i < 2; i++) {
		    	        // �Tenemos conexi�n? ponemos a true
		    	        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
		    	            bConectado = true;
		    	        }
		    	    }
		        	if(!bConectado)
		        	{
		        		
		        		 Toast.makeText(getBaseContext(),"Necesitas conexi�n a Internet ", Toast.LENGTH_SHORT)
		     		            .show();
		        	}	
		        	else
		        	{
		        	
				       ema=email.getText().toString();
				       con=contrasena.getText().toString();
				          

		         	String pepe;
		         	String politicas="false";
		         	
		         
		         	pepe=GET("http://54.72.215.193/api/metodos/login?mac="+myBlueToothAdapter.getAddress()+"&email="+ema+"&contrasena="+con);
		    		JSONObject json = null;
					try {
						
						json = new JSONObject(pepe);
						
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		    		
					

				if(pepe.startsWith("t"))
				{	        						
					 //Consulta para ver el perfil del usuario seleccionado en la tabla
			        pepe=GET("http://54.72.215.193/api/metodos/perfil/?mac="+myBlueToothAdapter.getAddress());
			      
			        try {
			              json = new JSONObject(pepe);
			          } catch (JSONException e) {
			              e.printStackTrace();
			          }
			        
			          if(json!=null)
			          {
				          try {
				            politicas=json.getString("visibilidad");
				           
				          
				             
				          } catch (JSONException e) {
				              e.printStackTrace();
				          }	          
				         
			          }

					//Comprobar si ya a aceptado las politicas de privacidad
					if(politicas.equals("1"))
					{
						Toast.makeText(getBaseContext(),"Espere por favor Cargando... ", Toast.LENGTH_SHORT)
     		            .show();
						Intent inten2 = new Intent(Blue.this,ListadomegustaActivity.class);
			        	startActivity(inten2);
			        	finish();			
					}
					else
					{
						Toast.makeText(Blue.this, "Cargando Politicas de privacidad...", Toast.LENGTH_LONG).show();
						Intent inten3 = new Intent(Blue.this,PoliticasActivity.class);
			        	startActivity(inten3);			
			        
					}
					
										
				}
				else if(pepe.startsWith("Lo"))
				{
					
					Toast.makeText(Blue.this, "Lo sentimos, error de conexi�n con el servidor", Toast.LENGTH_LONG).show();
					email.setText("");
		         	contrasena.setText("");			
				}			
				else
				{
					
					Toast.makeText(Blue.this, "Usuario o contrase�a incorrectas", Toast.LENGTH_LONG).show();
					email.setText("");
		         	contrasena.setText("");
					
				}
		        	}
	     		
		        	
		           
		        
		        
		        if(recordar.isChecked()) //Si esta seleccionado el tic.
    			{
    				GET("http://54.72.215.193/api/metodos/mod_recordar?mac="+myBlueToothAdapter.getAddress()+"&recordar=si");
    			       
    			}
    			else
    			{
    				GET("http://54.72.215.193/api/metodos/mod_recordar?mac="+myBlueToothAdapter.getAddress()+"&recordar=no");		
    			}
		      }
		       
			
			 });		
					
					
	}				


	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}


	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
	          if(httpResponse.getStatusLine()!=null)
	          {
		          // receive response as inputStream
		          inputStream = httpResponse.getEntity().getContent();
	
		          // convert inputstream to string
		          if(inputStream != null)
		              result = convertInputStreamToString(inputStream);
		          else
		              result = "Did not work!";
		          
		          }
	          else
	          {
	        	 result="Lo sentimos, error de conexi�n con el servidor";
	        	  
	          }

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }

	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;
	  }	  
}
