package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RecuperarActivity extends Activity {
	String ema="";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_recuperar);
		
		if (!verificaConexion(this)) {
		    Toast.makeText(getBaseContext(),
		            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
		    Intent inten2 = new Intent(RecuperarActivity.this,Blue.class);
            startActivity(inten2);
            finish();
		   
		}
		
		final Button si = (Button)findViewById(R.id.si);
		final Button no = (Button)findViewById(R.id.no);
		final BluetoothAdapter myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();	

		final EditText em=(EditText)findViewById(R.id.email);
		final TextView eti=(TextView)findViewById(R.id.eti);
		final TextView eti2=(TextView)findViewById(R.id.TextView01);
		final TextView eti3=(TextView)findViewById(R.id.etirecup);
				

		Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
		em.setTypeface(font);

		
		si.setTypeface(font);
		no.setTypeface(font);
		eti.setTypeface(font);
		eti2.setTypeface(font);
		eti3.setTypeface(font);
		
		
		

		
		
		
		String pepe;
		JSONObject json = null;
	 String email = null;
		 
        //Consulta para cargar el email para recordarselo al usuario.
        pepe=GET("http://54.72.215.193/api/metodos/perfil/?mac="+myBlueToothAdapter.getAddress());
 
              try {
				json = new JSONObject(pepe);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
          if(json!=null)
          {
	        	         	          
	           try {
				email=json.getJSONObject("profile").getString("email");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	           em.setText(email); 
	           ema=email;
          }
	          
             /* Botones
              * 
              *              
             */
				si.setOnClickListener(new OnClickListener()
				{
					public void onClick(View v)
			           {
						
						
						if (!verificaConexion(getBaseContext())) {
						    Toast.makeText(getBaseContext(),
						            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
						    Intent inten2 = new Intent(RecuperarActivity.this,Blue.class);
				            startActivity(inten2);
				            finish();
						   
						}
						else
						{
							Toast.makeText(getBaseContext(),"Se ha enviado a su correo la nueva contrase�a. ", Toast.LENGTH_SHORT).show();
							 //envio de correo al email del usuario
					       GET("http://54.72.215.193/api/metodos/pass/?mac="+myBlueToothAdapter.getAddress()+"&email="+ema);
					       
					       Intent inten2 = new Intent(RecuperarActivity.this,Blue.class);
			           	 	startActivity(inten2);
			           	 	finish();	
						
						}
						

			           
							
							
							
							
			           }
				
				 });	
				
				
				no.setOnClickListener(new OnClickListener()
				{
			        public void onClick(View v)
			           {
			        	
			            	Intent inten2 = new Intent(RecuperarActivity.this,Blue.class);
			           	 	startActivity(inten2);
			           	 	finish();	
			           }
				
				 });	
		
				
				}			
		
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_recuperar, menu);
		return true;
	}
	
	
	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

	          // receive response as inputStream
	          inputStream = httpResponse.getEntity().getContent();

	          // convert inputstream to string
	          if(inputStream != null)
	              result = convertInputStreamToString(inputStream);
	          else
	              result = "Did not work!";

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }

	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;

	  }
	  
	  public boolean verificaConexion(Context ctx) {
		    boolean bConectado = false;
		    ConnectivityManager connec = (ConnectivityManager) ctx
		            .getSystemService(Context.CONNECTIVITY_SERVICE);
		    // No s�lo wifi, tambi�n GPRS
		    NetworkInfo[] redes = connec.getAllNetworkInfo();
		    
		    for (int i = 0; i < 2; i++) {
		        // �Tenemos conexi�n? ponemos a true
		        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
		            bConectado = true;
		        }
		    }
		    return bConectado;
		}
	  

}
