package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemListadoAdapter extends BaseAdapter {
	protected Activity activity;
	protected ArrayList<ItemListado> items;
	final BluetoothAdapter myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
	
	
	public ItemListadoAdapter(Activity activity, ArrayList<ItemListado> items) {
		this.activity = activity;
		this.items = items;
	}


	@Override
	public int getCount() {
		return items.size();
	}


	@Override
	public Object getItem(int position) {
		return items.get(position);
	}


	@Override
	public long getItemId(int position) {
		return items.get(position).getId();
	}


	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
		
        if(convertView == null) {
        	LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        	vi = inflater.inflate(R.layout.list_item_layout, null);
        }
            
        ItemListado item = items.get(position);
        
        ImageView image = (ImageView) vi.findViewById(R.id.imagen);
        int imageResource = activity.getResources().getIdentifier(item.getRutaImagen(), null, activity.getPackageName());
        int anonimo=activity.getResources().getIdentifier("drawable/anonimo", null, activity.getPackageName());

        String pepe;
		
		 
		 JSONObject json = null;
	     JSONObject json2 = null;
	     String nom = null,est=null,cont=null;
	     String im=null;
	     JSONArray getJSONArray = null;
	     JSONArray array = null;
	       

		 //Cargo los datos dl perfil, pasandole la mac del usuario
		 pepe=GET("http://54.72.215.193/api/metodos/perfil/?mac="+item.getMac());
	      
	        try {
	              json = new JSONObject(pepe);
	          } catch (JSONException e) {
	              e.printStackTrace();
	          }
	        
	          if(json!=null)
	          {
		          try {
		           
		           im=json.getJSONObject("profile").getString("foto");
		             
		          } catch (JSONException e) {
		              e.printStackTrace();
		          }	          

  	   		         String nuevo=im.replaceAll("/", "%2F"); 	   		  
  	   		         String n2=nuevo.replace('+', '/'); 	   		         
  	   		         String n3=n2.replaceAll("/", "%2B");
		   	   		 String dd = null;
		   	   		     
		   	   		          
		   	   		 try {
						dd= URLDecoder.decode(n3, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		   	   		 
		   	   		 
		   	   		 if(im!=null)
		   	   		 {
			   	   		if(!im.equals("nada"))
			   	   			 image.setImageBitmap( redimensionarImagenMaximo(StringToBitMap2(dd), 200,200));
			   	   		 else			   	   		 
			   	   			 image.setImageDrawable(activity.getResources().getDrawable(imageResource));		   	   			 
		   	   		 }
		   	   		 else	   	   		 
		   	   			 image.setImageDrawable(activity.getResources().getDrawable(anonimo));	
		   	   			 
		   	   		 
		   	   		 
		   	   		 
		   	   		 
		   	   		 

	          }
        
        
        
     

        TextView nombre = (TextView) vi.findViewById(R.id.nombre);
        nombre.setText(item.getNombre());
        TextView contador = (TextView) vi.findViewById(R.id.contador);
        contador.setText(item.getcontador());
        TextView tipo = (TextView) vi.findViewById(R.id.tipo);
        tipo.setText(item.getTipo());

        
        
        Typeface font=Typeface.createFromAsset(activity.getAssets(), "MavenProLight-200.otf");
		nombre.setTypeface(font);
		contador.setTypeface(font);
		tipo.setTypeface(font);
        
        
       
        return vi;
	}


	public void clear() {
		// TODO Auto-generated method stub
		
		
		items.clear();
		
	}
	
	public Bitmap redimensionarImagenMaximo(Bitmap mBitmap, float newWidth, float newHeigth){
		   //Redimensionamos
		   int width = mBitmap.getWidth();
		   int height = mBitmap.getHeight();
		   float scaleWidth = ((float) newWidth) / width;
		   float scaleHeight = ((float) newHeigth) / height;
		   // create a matrix for the manipulation
		   Matrix matrix = new Matrix();
		   // resize the bit map
		   matrix.postScale(scaleWidth, scaleHeight);
		   // recreate the new Bitmap
		   return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
		}
	
	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

	          // receive response as inputStream
	          inputStream = httpResponse.getEntity().getContent();

	          // convert inputstream to string
	          if(inputStream != null)
	              result = convertInputStreamToString(inputStream);
	          else
	              result = "Did not work!";

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }

	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;

	  }
	public Bitmap StringToBitMap2(String encodedString){
	     try{
	    	 
	    	 BitmapFactory.Options options = new BitmapFactory.Options();	
		       options.inSampleSize = -2;
	    	 
	    	 
	       byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
	       Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
	       return bitmap;
	     }catch(Exception e){
	       e.getMessage();
	       return null;
	     }
}
}
