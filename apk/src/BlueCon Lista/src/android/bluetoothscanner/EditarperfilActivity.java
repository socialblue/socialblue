package android.bluetoothscanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;










import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

public class EditarperfilActivity extends Activity {

	
	 final BluetoothAdapter myBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
	 private int SELECT_PICTURE= 237487;
		private int TAKE_PICTURE = 829038;
		
		
		
		private String name="";
		
		 Uri currImageURI;
		
		 Button subir,server,ver;
		 EditText imagen;
		 ImageView ima, ima2 ;  
		 Bitmap bitmap;
		 Bitmap bitmapNuevo;
		 Intent intent; 
		 int code;
		
		 String g="nada",im;
		 String dd ;
		 ImageView fo,fo2;
		
		
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		String mac= mBluetoothAdapter.getAddress();
		 
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_editarperfil);
		
		final EditText contra=(EditText)findViewById(R.id.contra);
		final EditText contra2=(EditText)findViewById(R.id.contra2);
		
		Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
			contra.setTypeface(font);
			contra2.setTypeface(font);
		
	 
		Button edi= (Button) findViewById(R.id.editarPerfil);
		 final EditText estado=(EditText)findViewById(R.id.estado);
		 final EditText nombre=(EditText)findViewById(R.id.nombre);
		 TextView contador=(TextView)findViewById(R.id.contador);
		 fo=(ImageView)findViewById(R.id.foto);
		 
		 
		 
			estado.setTypeface(font);
			nombre.setTypeface(font);
			edi.setTypeface(font);
			contador.setTypeface(font);
		
		
			
			
			
			
		 String pepe;
		
		 
		 JSONObject json = null;
	     JSONObject json2 = null;
	     String nom = null,est=null,cont=null;
	     String im=null;
	     JSONArray getJSONArray = null;
	     JSONArray array = null;
	       

		 //Cargo los datos dl perfil, pasandole la mac del usuario
		 pepe=GET("http://54.72.215.193/api/metodos/perfil/?mac="+myBlueToothAdapter.getAddress());
	      

		 
	        try {
	              json = new JSONObject(pepe);
	          } catch (JSONException e) {
	              e.printStackTrace();
	          }
	        
	          if(json!=null)
	          {
		          try {
		           nom=json.getString("name");		          
		           est=json.getJSONObject("profile").getString("estado");
		           cont=json.getJSONObject("profile").getString("megusta");
		          
		           im=json.getJSONObject("profile").getString("foto");
		             
		          } catch (JSONException e) {
		              e.printStackTrace();
		          }	          
		          estado.setText(est.replaceAll("--"," "));
		          contador.setText(cont);
		          nombre.setText(nom.replaceAll("--"," "));
		          

		          

   	   		         String nuevo=im.replaceAll("/", "%2F"); 	   		  
   	   		         String n2=nuevo.replace('+', '/'); 	   		         
   	   		         String n3=n2.replaceAll("/", "%2B");
		   	   		 String dd = null;
		   	   		     
		   	   		          
		   	   		 try {
						dd= URLDecoder.decode(n3, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		   	   				   	   		 
		   	   	if(!im.equals("nada"))	   	   		 
	        	  fo.setImageBitmap(  redimensionarImagenMaximo(StringToBitMap2(dd), 150,150));		  	   			 	   	   		 
	   	   		 else	   	   		 
	   	   			fo.setImageResource(R.drawable.usuario); 		             

	          }

	        //Boton de editar
	  		edi.setOnClickListener(new OnClickListener()
	  		{
	  	        public void onClick(View v)
	  	           {	  	    
	  	        	
	  	        	if (!verificaConexion(getBaseContext())) {
	  	   			    Toast.makeText(getBaseContext(),
	  	   			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();        			    
	  	   			}
	  	        	else
	  	        	{
	  	        	
	  	        		
	  	        		if(!contra.getText().toString().equals(""))
	  	        		{
		  	          		String contrasena=contra.getText().toString().replaceAll(" ","--");
		  	          		String contrasena2=contra2.getText().toString().replaceAll(" ","--");
	  	          		
	  	          		
	  	          		
		  	          	
		  	          		
		  	          	if(contra.getText().toString().equals(contra2.getText().toString())	)	
	  	          		{
		  	          		if(contra.getText().toString().equals("contrasena")	&& contra2.getText().toString().equals("contrasena"))
		  	          		{
		  	          			GET("http://54.72.215.193/api/metodos/modestado/?mac="+myBlueToothAdapter.getAddress()+"&estado="+estado.getText().toString().replaceAll(" ","--")+"&nombre="+nombre.getText().toString().replaceAll(" ","--"));
		  	          			Toast.makeText(v.getContext(), "Editado con exito", Toast.LENGTH_SHORT).show();
		  	          			// Pantalla listadomegusta
		  	          			Intent inten2 = new Intent(v.getContext(),ListadomegustaActivity.class);
		  	          			startActivity(inten2);
		  	          			finish();	
		  	          		
		  	          		}
		  	          		else
		  	          		{
		  	          		Toast.makeText(v.getContext(), "Editado con exito", Toast.LENGTH_SHORT).show();
	  	      	         	 GET("http://54.72.215.193/api/metodos/mod_contrasena/?mac="+myBlueToothAdapter.getAddress()+"&contrasena="+contrasena);
	  	      	         	 GET("http://54.72.215.193/api/metodos/modestado/?mac="+myBlueToothAdapter.getAddress()+"&estado="+estado.getText().toString().replaceAll(" ","--")+"&nombre="+nombre.getText().toString().replaceAll(" ","--"));
	  	          			
	  	          			// Pantalla listadomegusta
	  	          			Intent inten2 = new Intent(v.getContext(),ListadomegustaActivity.class);
	  	          			startActivity(inten2);
	  	          			finish();	
	  	      	         	 
		  	          		}
	  	      	         	 
	  	          		}
	  	          		else
	  	          			 Toast.makeText(getBaseContext(),"No coinciden las contrase�as ", Toast.LENGTH_SHORT).show();
	  	          	
	  	          	}
	  	          	else
	  	          	{
	  	          		 Toast.makeText(getBaseContext(),"��Error!! Contrase�a en blanco ", Toast.LENGTH_SHORT).show();			        		
	  	          	}
	  	        		
	  	        		
	  	        		
	  	        		
	  	        	}
	  	           }
	  		
	  		 });
		 
		 
	

	
	  		//Imagen perfil
		fo.setOnClickListener(new OnClickListener()
		{
	        public void onClick(View v)
	           {
	        
	        	intent =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
	       		 code = TAKE_PICTURE;
	       		 
	       		
	       		 final CharSequence[] items = {"Seleccionar de la galer�a"};

	      		  AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
	      		  builder.setTitle("Seleccionar una foto");
	      		  builder.setItems(items, new DialogInterface.OnClickListener() {
	      		    public void onClick(DialogInterface dialog, int item) {
	      		      switch(item)
	      		      {
	      		       case 0:

	      		    	intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
	      				code = SELECT_PICTURE;
	      				startActivityForResult(intent, code);
	      				
	      		     break;
	      		       case 1: //camara
	      		    	   
	      		    	//Uri output = Uri.fromFile(new File(name));
	      		    	
	      		    	   // startActivityForResult(new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE), TAKE_PICTURE);  
	      		    	   
	      		    	   
	      		    	Uri output = Uri.fromFile(new File(name));
	      		    	intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
	      		    	startActivityForResult(intent, code);
	      		     break;
	      		      }
	      		            
	      		    }
	      		  });
	      		  AlertDialog alert = builder.create();
	      		  alert.show(); 

	           }
		
		 });
		
		
		
    	

        	
       }
       

		
 
		

	
	
	@Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	/**
    	 * Se revisa si la imagen viene de la c�mara (TAKE_PICTURE) o de la galer�a (SELECT_PICTURE)
    	 */
    	if (requestCode == TAKE_PICTURE) {
    		/**
    		 * Si se reciben datos en el intent tenemos una vista previa (thumbnail)
    		 */
    		if (data != null) {
    			/**
    			 * En el caso de una vista previa, obtenemos el extra �data� del intent y 
    			 * lo mostramos en el ImageView
    			 */
    			if (data.hasExtra("data")) { 
    				fo = (ImageView)findViewById(R.id.foto);
    				fo.setImageBitmap((Bitmap) data.getParcelableExtra("data"));
    			}
    		/**
    		 * De lo contrario es una imagen completa
    		 */    			
    		} else {
    			/**
    			 * A partir del nombre del archivo ya definido lo buscamos y creamos el bitmap
    			 * para el ImageView
    			 */
    			fo= (ImageView)findViewById(R.id.foto);
    			fo.setImageBitmap(BitmapFactory.decodeFile(name));
    			/**
    			 * Para guardar la imagen en la galer�a, utilizamos una conexi�n a un MediaScanner
    			 */
    			new MediaScannerConnectionClient() {
    				private MediaScannerConnection msc = null; {
    					msc = new MediaScannerConnection(getApplicationContext(), this); msc.connect();
    				}
    				public void onMediaScannerConnected() { 
    					msc.scanFile(name, null);
    				}
    				public void onScanCompleted(String path, Uri uri) { 
    					msc.disconnect();
    				} 
    			};				
    		}
    	/**
    	 * Recibimos el URI de la imagen y construimos un Bitmap a partir de un stream de Bytes
    	 */
    	} else if (requestCode == SELECT_PICTURE){
    		Uri selectedImage = data.getData();
    		InputStream is;
    		try {
    			
    			
    			
    			
    			
    			BitmapFactory.Options options = new BitmapFactory.Options();	
  		       options.inSampleSize = 16;
    			
    			is = getContentResolver().openInputStream(selectedImage);
    	    	BufferedInputStream bis = new BufferedInputStream(is);
    	    	bitmap = BitmapFactory.decodeStream(bis,null,options);
    	    	String j;
    	    	fo.setImageBitmap(  redimensionarImagenMaximo(bitmap, 150, 150));
    	    	
    	    	//SubirFoto nuevaTarea = new SubirFoto();
                //nuevaTarea.execute(bitmap);
    	    	
    	    	

    	    	g=BitMapToString2(bitmap);
    	    	
    	    	j=URLEncoder.encode(g, "UTF-8");		 
       		  
    	    	g=j;
    	    	
    	    	
    	    	

 	    	final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
 			final String mac= mBluetoothAdapter.getAddress();
 	    	
 			
		        	
		        	//Insertar en la base de datos la aceptaci�n de las politicas
		        	String pepe;
		        	  //Consulta para ver el perfil del usuario seleccionado en la tabla
			        pepe=GET("http://54.72.215.193/api/metodos/modfoto/?mac="+mac+"&estado="+g);
			        
			      
		        	
		        	
		        
				
    		} catch (FileNotFoundException e) {} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}

    	
    	}
	
	 public Bitmap StringToBitMap(String encodedString)
     {
         try{
           byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
           Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
           
           
           
          
           
           return bitmap;
         }catch(Exception e){
           e.getMessage();
           return null;
         }
          }

	
	
	@Override
	   public boolean onCreateOptionsMenu(Menu menu) {
	       MenuInflater inflater = getMenuInflater();
	       inflater.inflate(R.menu.menu_propiedades, menu);
	       return true;
	   }
	
	@Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      switch (item.getItemId()) {
	      case R.id.perfil:
	    	  if (!verificaConexion(this)) {
				    Toast.makeText(getBaseContext(),
				            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
	    	  }
	    	  else
	    	  {
	    		Intent in = new Intent(EditarperfilActivity.this,EditarperfilActivity .class);
          	startActivity(in);
	    	  }
	         return true;
	         
	      case R.id.bloqueados:
	    	  if (!verificaConexion(this)) {
				    Toast.makeText(getBaseContext(),
				            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
	    	  }
	    	  else
	    	  {
	    		Intent in2 = new Intent(EditarperfilActivity.this,ListadobloqueadosActivity .class);
	    		startActivity(in2);
	    	  }   
          	return true;
	 
	      case R.id.salir:
	         finish();
	         return true;
	 
	      default:
	         return super.onOptionsItemSelected(item);
	      }
	   }
	
	
	
	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

	          // receive response as inputStream
	          inputStream = httpResponse.getEntity().getContent();

	          // convert inputstream to string
	          if(inputStream != null)
	              result = convertInputStreamToString(inputStream);
	          else
	              result = "Did not work!";

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }

	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;

	  }
	  
	
	  public String BitMapToString2(Bitmap bitmap){
	         

			 ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	         bitmap.compress(Bitmap.CompressFormat.PNG,8, baos);
	         
	         byte [] b=baos.toByteArray();
	         String temp=Base64.encodeToString(b, Base64.DEFAULT);
	         return temp;
	   }
		  
		 
		  public Bitmap StringToBitMap2(String encodedString){
			     try{
			       byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
			       
			    
			    		   BitmapFactory.Options options = new BitmapFactory.Options();	
	 		               options.inSampleSize =0;
	 		       
	 		      
	 		       
			       Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length,options);
			       
			       
			       return bitmap;
			     }catch(Exception e){
			       e.getMessage();
			       return null;
			     }
			      }
	 
	  public String BitMapToString(Bitmap bitmap){
          ByteArrayOutputStream baos=new  ByteArrayOutputStream();
          bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
          byte [] b=baos.toByteArray();
          String temp=Base64.encodeToString(b, Base64.DEFAULT);
          return temp;
    }
	 
	  public Bitmap redimensionarImagenMaximo(Bitmap mBitmap, float newWidth, float newHeigth){
		   //Redimensionamos
		   int width = mBitmap.getWidth();
		   int height = mBitmap.getHeight();
		   float scaleWidth = ((float) newWidth) / width;
		   float scaleHeight = ((float) newHeigth) / height;
		   // create a matrix for the manipulation
		   Matrix matrix = new Matrix();
		   // resize the bit map
		   matrix.postScale(scaleWidth, scaleHeight);
		   // recreate the new Bitmap
		   return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
		}
	  
	  
	  public boolean verificaConexion(Context ctx) {
		    boolean bConectado = false;
		    ConnectivityManager connec = (ConnectivityManager) ctx
		            .getSystemService(Context.CONNECTIVITY_SERVICE);
		    // No s�lo wifi, tambi�n GPRS
		    NetworkInfo[] redes = connec.getAllNetworkInfo();
		    
		    for (int i = 0; i < 2; i++) {
		        // �Tenemos conexi�n? ponemos a true
		        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
		            bConectado = true;
		        }
		    }
		    return bConectado;
		}
	

}
