package android.bluetoothscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PoliticasActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_politicas);
		
		final Button aceptar = (Button)findViewById(R.id.aceptar);
		final Button cancelar = (Button)findViewById(R.id.cancelar);
		final TextView area= (TextView)findViewById(R.id.area);
		
		Typeface font=Typeface.createFromAsset(getAssets(), "MavenProLight-200.otf");
		aceptar.setTypeface(font);
		cancelar.setTypeface(font);
		area.setTypeface(font);
		
		
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		final String mac= mBluetoothAdapter.getAddress();
		
		
				aceptar.setOnClickListener(new OnClickListener()
				{
			        public void onClick(View v)
			           {
			        	if (!verificaConexion(getBaseContext())) {
			  			    Toast.makeText(getBaseContext(),
			  			            "Necesitas conexi�n a Internet. ", Toast.LENGTH_SHORT).show();
			  			    Intent inten2 = new Intent(PoliticasActivity.this,Blue.class);
			  	            startActivity(inten2);
			  	            finish();
			  			   
			  			}
			  			
			        	//Insertar en la base de datos la aceptaci�n de las politicas
			        	String pepe;
			        	  //Consulta para ver el perfil del usuario seleccionado en la tabla
				        pepe=GET("http://54.72.215.193/api/metodos/politica/?mac="+mac+"&estado="+1);
				        
			        	Intent inten = new Intent(PoliticasActivity.this,ListadomegustaActivity.class);
			        	startActivity(inten);
			        	finish();
			           }
				
				 });		
				
				cancelar.setOnClickListener(new OnClickListener()
				{
			        public void onClick(View v)
			           {
			        		Toast.makeText(PoliticasActivity.this, "Debe aceptar las pol�ticas de privacidad para continuar", 
			        		Toast.LENGTH_LONG).show();
			        	

				        	Intent inten = new Intent(PoliticasActivity.this,Blue.class);
				        	startActivity(inten);
				        	finish();
			           }
				
				 });		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_politicas, menu);
		return true;
	
		
	}
	
	public static String GET(String url){
	      InputStream inputStream = null;
	      String result = "";
	      try {

	          // create HttpClient
	          HttpClient httpclient = new DefaultHttpClient();
	          httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
	          // make GET request to the given URL
	          HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

	          // receive response as inputStream
	          inputStream = httpResponse.getEntity().getContent();

	          // convert inputstream to string
	          if(inputStream != null)
	              result = convertInputStreamToString(inputStream);
	          else
	              result = "Did not work!";

	      } catch (Exception e) {
	          Log.d("InputStream", e.getLocalizedMessage());
	      }

	      return result;
	  }
	 private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	      BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	      String line = "";
	      String result = "";
	      while((line = bufferedReader.readLine()) != null)
	          result += line;

	      inputStream.close();
	      return result;

	  }
	 
	 public boolean verificaConexion(Context ctx) {
		    boolean bConectado = false;
		    ConnectivityManager connec = (ConnectivityManager) ctx
		            .getSystemService(Context.CONNECTIVITY_SERVICE);
		    // No s�lo wifi, tambi�n GPRS
		    NetworkInfo[] redes = connec.getAllNetworkInfo();
		    
		    for (int i = 0; i < 2; i++) {
		        // �Tenemos conexi�n? ponemos a true
		        if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
		            bConectado = true;
		        }
		    }
		    return bConectado;
		}

}
